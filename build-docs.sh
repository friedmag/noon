#!/bin/sh

ROOTDIR=$(cd $(dirname $0); pwd)

cd $ROOTDIR
rm -rf doc/generated
doxygen doc/doxygen.conf
#USENAME=$(basename $ROOTDIR)
#(tar -C doc/generated/html -zcf - .) | ssh aws "cd /var/www/doc; rm -rf $USENAME; mkdir $USENAME; cd $USENAME; tar zxf -"
