#include <noon-promise.hpp>
#include <catch2/catch.hpp>

class VoidPromise
{
public:
  using PromiseType = noon::basic_promise<>;

  PromiseType::Resolve mResolve;
  PromiseType::Error mError;
  PromiseType::Ptr mPromise;

  VoidPromise() {};

  PromiseType::Ptr call()
  {
    // NOTE: This Promise must be kept around until resolved/error'd
    mPromise = std::make_shared<PromiseType>([=](PromiseType::Resolve aResolve, PromiseType::Error aError) {
      mResolve = aResolve;
      mError = aError;
    });
    return mPromise;
  };

  void resolve()
  {
    mResolve();
  };

  void error(std::exception aVal)
  {
    mError(aVal);
  };
};

class IntPromise
{
public:
  using PromiseType = noon::basic_promise<int>;

  PromiseType::Resolve mResolve;
  PromiseType::Error mError;
  PromiseType::Ptr mPromise;

  IntPromise() {};

  PromiseType::Ptr call()
  {
    // NOTE: This Promise must be kept around until resolved/error'd
    mPromise = std::make_shared<PromiseType>([=](PromiseType::Resolve aResolve, PromiseType::Error aError) {
      mResolve = aResolve;
      mError = aError;
    });
    return mPromise;
  };

  void resolve(int aVal)
  {
    mResolve(aVal);
  };

  void error(std::exception aVal)
  {
    mError(aVal);
  };
};
