#include "test_promise_common.hpp"

SCENARIO("Promise INT test", "[promise]") {

  using P = noon::basic_promise<int>;
  using P2 = noon::basic_promise<>;

  GIVEN("An async class providing promises with an int value") {
    IntPromise tMain;
    VoidPromise tAlt;
    int tCalled = 0;
    int tVal = 0;

    // Unique stuff - value return
    WHEN("a value is returned") {
      auto t = tMain.call()
        ->then([&](auto) {
          REQUIRE(tCalled == 0);
          ++tCalled;
          return std::make_tuple(42);
        });
      THEN("it is passed along") {
        t->then([&](auto aVal) {
            REQUIRE(aVal == 42);
            tCalled += aVal;
            return nullptr;
          });
        REQUIRE(tCalled == 0);
        tMain.resolve(0);
        REQUIRE(tCalled == 43);
      }
      AND_WHEN("it throws an error") {
        t->then([&](auto) {
            throw std::exception();
            return std::make_tuple(42);
          });
        THEN("that error is rethrown") {
          REQUIRE_THROWS_AS(tMain.resolve(1), std::exception);
        }
      }
    }

    // Generic stuff
    WHEN("given a single call") {
      tMain.call()
        ->then([&](int aVal) {
          REQUIRE(tCalled == 0);
          REQUIRE(aVal == 0);
          ++tCalled;
          tVal = aVal + 1;
          return nullptr;
        });

      REQUIRE(tCalled == 0);

      AND_WHEN("the value is resolved") {
        THEN("'then' is run") {
          tMain.resolve(0);
          REQUIRE(tCalled == 1);
          REQUIRE(tVal == 1);
        }
      }

      AND_WHEN("the value is resolved twice") {
        THEN("it errors") {
          tMain.resolve(0);
          REQUIRE(tCalled == 1);
          //REQUIRE_THROWS(tMain.resolve()); // TODO: make this throw rather than crash
        }
      }
    }

    WHEN("multiple calls are chained via promises") {
      THEN("they are called as resolved") {
        tMain.call()
          ->then([&](int aVal) {
            REQUIRE(tCalled == 0);
            REQUIRE(aVal == 1);
            ++tCalled;
            return tMain.call();
          })
          ->then([&](int aVal) {
            REQUIRE(tCalled == 1);
            REQUIRE(aVal == 42);
            ++tCalled;
            return tMain.call();
          });
        REQUIRE(tCalled == 0);
        tMain.resolve(1);
        REQUIRE(tCalled == 1);
        tMain.resolve(42);
        REQUIRE(tCalled == 2);
      }
    }

    WHEN("multiple calls are chained via null promises") {
      THEN("they are called in sequence") {
        tMain.call()
          ->then([&](int aVal) {
            REQUIRE(tCalled == 0);
            REQUIRE(aVal == 42);
            ++tCalled;
            return nullptr;
          })
          ->then([&](int aVal) {
            REQUIRE(tCalled == 1);
            REQUIRE(aVal == 42); // prev value is passed through on null
            ++tCalled;
            return nullptr;
          });
        REQUIRE(tCalled == 0);
        tMain.resolve(42);
        REQUIRE(tCalled == 2);
      }
    }

    WHEN("an exception occurs in then") {
      auto t = tMain.call()
        ->then([&](int) {
          throw std::exception(); //std::exception();
          return tMain.call();
        });
      AND_WHEN("it has no error case") {
        THEN("it rethrows the exception") {
          REQUIRE_THROWS_AS(tMain.resolve(1), std::exception);
        }
      }
      AND_WHEN("it has an error case") {
        THEN("it does not rethrow") {
          t->error([&](std::exception) {
              ++tCalled;
              return nullptr;
            });
          REQUIRE(tCalled == 0);
          REQUIRE_NOTHROW(tMain.resolve(1));
          REQUIRE(tCalled == 1);
        }
      }
    }

    WHEN("an error occurs via promise class") {
      AND_WHEN("there is just a handler") {
        THEN("the error handler is called") {
          tMain.call()
            ->error([&](std::exception) {
              REQUIRE(tCalled == 0);
              tCalled = 42;
              return nullptr;
            });
          tMain.error(std::exception());
          REQUIRE(tCalled == 42);
        }
      }
      AND_WHEN("there are thens before a handler") {
        THEN("the error handler is called directly") {
          int tErr = 0;
          tMain.call()
            ->then([&](int) {
              REQUIRE(tCalled == 0);
              ++tCalled;
              return tMain.call();
            })
            ->error([&](std::exception) {
              REQUIRE(tCalled == 0);
              ++tCalled;
              tErr = 42;
              return nullptr;
            });
          REQUIRE(tErr == 0);
          tMain.error(std::exception());
          REQUIRE(tCalled == 1);
          REQUIRE(tErr == 42);
        }
      }
      AND_WHEN("there an error occurs in the middle of processing") {
        THEN("pending thens are skipped, going to the error handler") {
          int tErr = 0;
          tMain.call()
            ->then([&](int) {
              REQUIRE(tCalled == 0);
              ++tCalled;
              return tMain.call();
            })
            ->then([&](int) {
              REQUIRE(tCalled == 1);
              ++tCalled;
              return tMain.call();
            })
            ->error([&](std::exception) {
              REQUIRE(tCalled == 1);
              --tCalled;
              tErr = 42;
              return nullptr;
            });
          REQUIRE(tCalled == 0);
          tMain.resolve(0);
          REQUIRE(tCalled == 1);
          REQUIRE(tErr == 0);
          tMain.error(std::exception());
          REQUIRE(tCalled == 0);
          REQUIRE(tErr == 42);
        }
      }
    }

    WHEN("an error is handled") {
      auto t = tMain.call()
        ->then([&](int) {
          throw std::exception(); //std::exception();
          return tMain.call();
        });
      AND_WHEN("the error handler throws, resulting in the same type") {
        auto t2 = t->error([&](std::exception) {
            throw std::exception();
            ++tCalled;
            return nullptr;
          });
        AND_WHEN("it is not handled") {
            THEN("the exception is rethrown") {
            REQUIRE(tCalled == 0);
            REQUIRE_THROWS_AS(tMain.resolve(1), std::exception);
            REQUIRE(tCalled == 0);
          }
        }
        AND_WHEN("it is handled") {
          t2->error([&](std::exception) {
              --tCalled;
              return nullptr;
            });
          THEN("the exception is NOT rethrown") {
            REQUIRE(tCalled == 0);
            REQUIRE_NOTHROW(tMain.resolve(0));
            REQUIRE(tCalled == -1);
          }
        }
      }
      AND_WHEN("the error handler throws, resulting in a different type") {
        auto t2 = t->error<>([&](std::exception) {
            throw std::exception();
            ++tCalled;
            return nullptr;
          })
          ->error([&](std::exception) {
              --tCalled;
              return nullptr;
            });
        THEN("the exception is NOT rethrown") {
          REQUIRE(tCalled == 0);
          REQUIRE_NOTHROW(tMain.resolve(0));
          REQUIRE(tCalled == -1);
        }
      }
      AND_WHEN("it returns nullptr of the same type") {
        t->error([&](std::exception) {
            ++tCalled;
            return nullptr;
          })
          ->then([&](int) {
            ++tCalled;
            return nullptr;
          });
        THEN("processing will continue immediately") {
          REQUIRE(tCalled == 0);
          REQUIRE_NOTHROW(tMain.resolve(1));
          REQUIRE(tCalled == 2);
        }
      }
      AND_WHEN("it returns nullptr of a different type") {
        t->error<>([&](std::exception) {
            ++tCalled;
            return nullptr;
          })
          ->then([&]() {
            ++tCalled;
            return nullptr;
          });
        THEN("processing will continue immediately") {
          REQUIRE(tCalled == 0);
          REQUIRE_NOTHROW(tMain.resolve(0));
          REQUIRE(tCalled == 2);
        }
      }
      AND_WHEN("it returns a promise") {
        THEN("processing will continue when resolved") {
        }
      }
      AND_WHEN("it returns a value") {
        THEN("processing will continue") {
        }
      }
    }

    WHEN("a promise goes unresolved") {
      THEN("it should still not leak") {
        // TODO: figure out how to make this not leak
        // TODO: can this be tested for without looking at results in valgrind?
        tMain.call()
          ->then([&](int) {
            return nullptr;
          });
      }
    }

    WHEN("chained to a different promise type") {
      AND_WHEN("it works") {
        THEN("it works") {
          tMain.call()
            ->then<>([&](int) {
              ++tCalled;
              return tAlt.call();
            })
            ->then([&]() {
              ++tCalled;
              return nullptr;
            });
          REQUIRE(tCalled == 0);
          tMain.resolve(0);
          REQUIRE(tCalled == 1);
          tAlt.resolve();
          REQUIRE(tCalled == 2);
        }
      }

      AND_WHEN("it throws an error") {
        THEN("it gets rethrown") {
          REQUIRE(tCalled == 0);
          REQUIRE_THROWS_AS([&](){
            tMain.call()
              ->then<>([&](int) {
                ++tCalled;
                throw std::exception();
                ++tCalled;
                return tAlt.call();
              })
              ->then([&]() {
                ++tCalled;
                return nullptr;
              });
            tMain.resolve(0);
            tAlt.resolve();
          }(), std::exception);
          REQUIRE(tCalled == 1);
        }
      }

      AND_WHEN("it handles an error") {
        THEN("it continues on") {
          tMain.call()
            ->error<>([&](std::exception) {
              ++tCalled;
              return tAlt.call();
            })
            ->then([&]() {
              ++tCalled;
              return nullptr;
            });
          REQUIRE(tCalled == 0);
          tMain.error(std::exception());
          REQUIRE(tCalled == 1);
          tAlt.resolve();
          REQUIRE(tCalled == 2);
        }
      }
    }

    WHEN("an error chains to a same-typed wrapper") {
      auto t = tMain.call()
        ->error([&](std::exception) {
          ++tCalled;
          throw std::exception();
          ++tCalled;
          return P::create([&](P::Resolve, P::Error) {
            });
        });
      REQUIRE(tCalled == 0);
      AND_WHEN("it is not handled") {
        REQUIRE_THROWS_AS(tMain.error(std::exception()), std::exception);
        REQUIRE(tCalled == 1);
      }
      AND_WHEN("it is handled") {
        t->error([&](std::exception) {
            return nullptr;
          });
        REQUIRE_NOTHROW(tMain.error(std::exception()));
        REQUIRE(tCalled == 1);
      }
    }

    WHEN("an error chains to a differently-typed wrapper") {
      auto t = tMain.call()
        ->error<>([&](std::exception) {
          ++tCalled;
          throw std::exception();
          ++tCalled;
          return P2::create([&](P2::Resolve, P2::Error) {
            });
        });
      REQUIRE(tCalled == 0);
      AND_WHEN("it is not handled") {
        REQUIRE_THROWS_AS(tMain.error(std::exception()), std::exception);
        REQUIRE(tCalled == 1);
      }
      AND_WHEN("it is handled") {
        t->error([&](std::exception) {
            return nullptr;
          });
        REQUIRE_NOTHROW(tMain.error(std::exception()));
        REQUIRE(tCalled == 1);
      }
    }

    WHEN("chained to a same-typed wrapper") {
    }

    WHEN("chained to a different-typed wrapper") {
    }
  }

  GIVEN("An asynchronous operation w/o promises") {
    int tVal = 0;

    WHEN("wrapped") {
      THEN("it works") {
        P::create([&](P::Resolve aResolve, P::Error) {
          aResolve(19);
        })
          ->then([&](int aVal) {
            REQUIRE(aVal == 19);
            REQUIRE(tVal == 0);
            tVal = 42;
            return nullptr;
          });
        REQUIRE(tVal == 42);
      }
    }

    WHEN("an error occurs") {
      THEN("it is caught") {
        REQUIRE_THROWS_AS([&](){
          P::create([&](P::Resolve aResolve, P::Error) {
            aResolve(19);
          })
            ->then([&](int) {
              throw std::exception();
              return P::create([&](P::Resolve aResolve, P::Error) {
                aResolve(19);
              });
            });
        }(), std::exception);
      }
    }

    WHEN("a wrapper calls a differently-typed wrapper, that fails") {
      THEN("it is caught") {
        REQUIRE_THROWS_AS([&](){
          P::create([&](P::Resolve aResolve, P::Error) {
            aResolve(19);
          })
            ->then<>([&](int) {
              throw std::exception();
              return P2::create([&](P2::Resolve aResolve, P2::Error) {
                aResolve();
              });
            });
        }(), std::exception);
      }
    }

    WHEN("a wrapper calls a differently-typed wrapper") {
      AND_WHEN("it throws an error") {
        THEN("it is caught") {
          // TODO: fix
//          REQUIRE_THROWS_AS({
//            P::create([&](P::Resolve aResolve, P::Error) {
//              aResolve(0);
//            })
//              ->then<void>([&](int) {
//                throw std::exception();
//                return P2::create([&](P2::Resolve aResolve, P2::Error) {
//                  aResolve();
//                });
//              });
//          }, std::exception);
        }
      }

      AND_WHEN("it does not throw an error") {
        THEN("it succeeds") {
        }
      }
    }

    WHEN("a wrapper calls a wrapper") {
      THEN("it works") {
        REQUIRE(tVal == 0);
        P::create([&](P::Resolve aResolve, P::Error) {
          aResolve(19);
        })
          ->then([&](int aVal) {
            REQUIRE(aVal == 19);
            return P::create([&](P::Resolve aResolve, P::Error) {
              aResolve(42);
            });
          })
          ->then([&](int aVal) {
            REQUIRE(aVal == 42);
            tVal = aVal;
            return nullptr;
          });
        REQUIRE(tVal == 42);
      }
    }
  }
}
