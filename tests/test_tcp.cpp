#include <noon-uv-socket.hpp>
#include <noon-uv-loop.hpp>
#include <catch.hpp>

SCENARIO("TCP should have basic functionality", "[tcp][stream][socket]") {
  GIVEN("A loop with a TCP socket") {
    // TODO: Add test for a hostname to bind/connect to
    noon::socket::Address tAddr("127.0.0.1", 32345);
    noon::Loop tLoop;
    noon::tcp tTcp(tLoop);
    WHEN("it is bound to an IP") {
      auto tStatus = tTcp.bind(tAddr);
      THEN("it should not error out") {
        if (tStatus)
          FAIL("Failed in bind: " << tStatus.what());
      }
      AND_THEN("it should have the given address") {
        REQUIRE(tTcp.address() == tAddr);
      }
      AND_WHEN("it is listening") {
        std::unique_ptr<noon::tcp> tAccept = nullptr;
        tStatus = tTcp.listen([&](auto e) {
          if (e)
            FAIL("Failed in listen: " << e.what());
          tAccept = tTcp.accept<noon::tcp>();
          tTcp.close();
        });
        REQUIRE_FALSE(tStatus);
        AND_THEN("someone else tries to bind/listen") {
          noon::tcp tOtherTcp(tLoop);
          tStatus = tOtherTcp.bind(tAddr);
          REQUIRE_FALSE(tStatus); // I'd actually like it if this failed...
          tStatus = tOtherTcp.listen([&](auto e) {
            if (e)
              FAIL("Failed in secondary listen: " << e.what());
          });
          THEN("it should fail") {
            REQUIRE(tStatus);
          }
        }
        THEN("someone connects") {
          noon::tcp tOtherTcp(tLoop);
          tOtherTcp.connect(tAddr)
            ->error([&](auto e) {
              FAIL("Failed in connect: " << e.what());
              return nullptr;
            });
          tLoop.run();
          REQUIRE(tAccept != nullptr);
          AND_THEN("someone sends a message") {
            std::string tMessage = "hello, tcp!";
            std::string tRead;
            tAccept->read([&](auto e, auto aBuf, auto) {
              if (e)
                FAIL("Failed to read: " << e.what());
              tRead = aBuf->data;
              tAccept->readStop();
            });

            WHEN("They have a callback") {
              tOtherTcp.write(tMessage, [&](auto e) {
                if (e)
                  FAIL("Failed to write: " << e.what());
              });
              while (tLoop.run());
              THEN("the message should succeed") {
                REQUIRE(tRead == tMessage);
              }
            }

            AND_WHEN("They do NOT have a callback") {
              tOtherTcp.write(tMessage);
              while (tLoop.run());
              THEN("the message should succeed") {
                REQUIRE(tRead == tMessage);
              }
            }

            AND_WHEN("data goes the other way") {
              tOtherTcp.write(tMessage);
              while (tLoop.run());
              REQUIRE(tRead == tMessage);
              tMessage = "hello to you as well!";
              tRead = "";
              REQUIRE(tRead.empty());
              tOtherTcp.read([&](auto e, auto aBuf, auto) {
                if (e)
                  FAIL("Failed to read: " << e.what());
                tRead = aBuf->data;
                tOtherTcp.readStop();
              });
              tAccept->write(tMessage, [&](auto e) {
                if (e)
                  FAIL("Failed to write: " << e.what());
              });
              while (tLoop.run());
              REQUIRE(tRead == tMessage);
            }
          }

          AND_THEN("peer is available") {
            auto tPeer = tOtherTcp.peer();
            REQUIRE(tPeer.addr != "");
            REQUIRE(tPeer.port > 0);
          }
        }
      }
    }
  }
}
