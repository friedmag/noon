#include <noon-uv-socket.hpp>
#include <noon-uv-loop.hpp>
#include <catch.hpp>

SCENARIO("UDP should have basic functionality", "[udp][socket]") {
  GIVEN("A loop with a UDP socket") {
    // TODO: Add test for a hostname to bind/connect to
    noon::socket::Address tAddr("127.0.0.1", 32345);
    noon::Loop tLoop;
    noon::udp tUdp(tLoop);
    WHEN("it is bound to an IP") {
      auto tStatus = tUdp.bind(tAddr);
      THEN("it should not error out") {
        if (tStatus)
          FAIL("Failed in bind: " << tStatus.what());
      }
      AND_THEN("it should have the given address") {
        REQUIRE(tUdp.address() == tAddr);
      }
      AND_WHEN("it is reading data") {
        std::string tRead;
        std::string tMessage;
        noon::udp tOtherUdp(tLoop);
        // TODO: Test two guys listening on same port, both should get message
        THEN("someone sends data") {
          tUdp.read([&](auto e, auto aBuf, auto) {
            if (e)
              FAIL("Failed to read: " << e.what());
            tRead = aBuf->data;
            tUdp.readStop();
          });
          tMessage = "hello, udp!";
          tOtherUdp.target(tAddr);
          tOtherUdp.write(tMessage, [&](auto e) {
            if (e)
              FAIL("Failed to write: " << e.what());
          });
          tLoop.run();
          REQUIRE(tRead.size() == tMessage.size());
          REQUIRE(tRead == tMessage);
        }
        AND_THEN("it goes the other way, too!") {
          noon::socket::Address tOtherAddr("127.0.0.1", 43123);
          tMessage = "hello to you as well!";
          REQUIRE(tRead.empty());
          tStatus = tOtherUdp.bind(tOtherAddr);
          REQUIRE_FALSE(tStatus);
          tOtherUdp.read([&](auto e, auto aBuf, auto) {
            if (e)
              FAIL("Failed to read: " << e.what());
            tRead = aBuf->data;
            tOtherUdp.readStop();
          });
          tUdp.target(tOtherAddr);
          tUdp.write(tMessage, [&](auto e) {
            if (e)
              FAIL("Failed to write: " << e.what());
          });
          tLoop.run();
          REQUIRE(tRead.size() == tMessage.size());
          REQUIRE(tRead == tMessage);

          AND_THEN("peer is set, too") {
            auto tPeer = tUdp.peer();
            REQUIRE(tPeer.addr == tOtherAddr.addr);
            REQUIRE(tPeer.port == tOtherAddr.port);
          }
        }
      }
    }
  }
}
