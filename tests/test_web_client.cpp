#include <noon-web.hpp>
#include <catch.hpp>

static constexpr uint16_t sPort = 23142;

SCENARIO("Client test", "[web] [client]") {
  GIVEN("A server") {
    noon::Loop tLoop;
    noon::web_server tServer(tLoop);
    std::function<void(noon::web_server_response_ptr)> tGetFunc;
    std::function<void(noon::web_server_request_ptr, noon::web_server_response_ptr)> tPostFunc;
    bool tRequested = false;
    
    auto common = [&]() {
      tLoop.run();
      REQUIRE(tRequested);
    };

    tServer.listen(sPort);
    tServer.get("/hello", [&](auto, auto aRes) {
        tRequested = true;
        if (tGetFunc) tGetFunc(aRes);
      });
    tServer.post("/hello", [&](auto aReq, auto aRes) {
        tRequested = true;
        if (tPostFunc) tPostFunc(aReq, aRes);
      });
      
    noon::web tClient(tLoop);
    std::stringstream tUrl;
    tUrl << "http://localhost:" << sPort;

    WHEN("accessing /hello") {
      tUrl << "/hello";
      auto tConn = tClient.request(noon::map<std::string>("url", tUrl.str()));
      std::string tResponse("goodbye");
  
      noon::web_client_response_ptr tRes;
      tConn->response()->then<>([&](auto aRes) {
          tRes = aRes;
          tServer.close();
          return nullptr;
        });
  
      WHEN("it gives a simple response on /hello") {
        tGetFunc = [&](auto aRes) {
          aRes->end(tResponse);
        };
  
        THEN("it receives the proper response") {
          common();
          REQUIRE(tRes->body == tResponse);
          REQUIRE(tRes->statusCode == 200);
          REQUIRE(tRes->headers["Connection"] == "close");
        }
      }
  
      WHEN("it sets headers on /hello") {
        tGetFunc = [&](auto aRes) {
          aRes->setHeader("X-Set-Test", "true");
          aRes->end(tResponse);
        };
  
        THEN("the client receives the headers") {
          common();
          REQUIRE(tRes->body == tResponse);
          REQUIRE(tRes->statusCode == 200);
          REQUIRE(tRes->headers["Connection"] == "close");
          REQUIRE(tRes->headers["X-Set-Test"] == "true");
        }
      }
  
      WHEN("it writes w/ headers on /hello") {
        tGetFunc = [&](auto aRes) {
          aRes->writeHead(noon::map<std::string, std::string>("X-Set-Test", "true"));
          aRes->end(tResponse);
          aRes->close(); // force close - header not set
        };
  
        THEN("the client receives the headers") {
          common();
          REQUIRE(tRes->body == tResponse);
          REQUIRE(tRes->statusCode == 200);
          REQUIRE(tRes->headers["Connection"] == "");
          REQUIRE(tRes->headers["X-Set-Test"] == "true");
        }
      }
  
      WHEN("it removes a header on /hello") {
        tGetFunc = [&](auto aRes) {
          aRes->removeHeader("Connection");
          aRes->end(tResponse);
          aRes->close(); // force close - header not set
        };
  
        THEN("the client does not the header") {
          common();
          REQUIRE(tRes->body == tResponse);
          REQUIRE(tRes->statusCode == 200);
          REQUIRE(tRes->headers["Connection"] == "");
          REQUIRE(tRes->headers["X-Set-Test"] == "");
        }
      }
  
      WHEN("it sets status on /hello") {
        tGetFunc = [&](auto aRes) {
          aRes->statusCode = 201;
          aRes->end(tResponse);
        };
  
        THEN("the client gets the status") {
          common();
          REQUIRE(tRes->body == tResponse);
          REQUIRE(tRes->statusCode == 201);
        }
      }
  
      WHEN("an 'unknown' status is given on /hello") {
        tGetFunc = [&](auto aRes) {
          aRes->statusCode = 299;
          aRes->end(tResponse);
        };
  
        THEN("the client gets the status") {
          common();
          REQUIRE(tRes->body == tResponse);
          REQUIRE(tRes->statusCode == 299);
        }
      }
    }

    WHEN("posting to /hello") {
      noon::web_client tWClient{tLoop};
      tUrl << "/hello";
      std::string tData("governor");
      noon::web_client_response_ptr tRes;
  
      WHEN("data is posted on /hello, data can be processed and returned") {
        auto tConn = tWClient.post(tUrl.str(), std::move(tData));
        tPostFunc = [&](auto aReq, auto aRes) {
          aReq->onData([=](std::string aData) {
            std::stringstream tResp;
            tResp << "hello " << aData;
            aRes->end(tResp.str());
          });
        };

        tConn->then<>([&](auto aRes) {
          tRes = aRes;
          tServer.close();
          tRequested = true;
          return nullptr;
        });
  
        THEN("the client gets the status") {
          common();
          REQUIRE(tRes->body == "hello governor");
        }
      }
    }
    
    WHEN("an unrouted endpoint should return 404") {
      tUrl << "/goodbye";
      auto tConn = tClient.request(noon::map<std::string>("url", tUrl.str()));
      
      noon::web_client_response_ptr tRes;
      tConn->response()->then<>([&](auto aRes) {
        tRes = aRes;
        tServer.close();
        tRequested = true;
        return nullptr;
      });

      THEN("the client gets the status") {
        common();
        REQUIRE(tRes->statusCode == 404);
      }
    }
  }
}
