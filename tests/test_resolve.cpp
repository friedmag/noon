#include <noon-uv-socket.hpp>
#include <noon-uv-loop.hpp>
#include <catch.hpp>

SCENARIO("Resolving hostnames", "[udp][resolve][socket]") {
  GIVEN("A loop with a UDP socket") {
    noon::Loop tLoop;
    noon::udp tUdp(tLoop);

    WHEN("resolving localhost4") {
      std::string tName;
      REQUIRE(tName.empty());
      tUdp.resolve("localhost4")
        ->then([&](auto aIP) {
          tName = aIP;
          return noon::ResolvePromise::Ptr();
        });
      tLoop.run();
      THEN("it resolved correctly") {
        REQUIRE(tName == "127.0.0.1");
      }
    }

    WHEN("resolving localhost6") {
      std::string tName;
      REQUIRE(tName.empty());
      tUdp.resolve("localhost6")
        ->then([&](auto aIP) {
          tName = aIP;
          return noon::ResolvePromise::Ptr();
        });
      tLoop.run();
      THEN("it resolved correctly") {
        REQUIRE(tName == "::");
      }
    }

    WHEN("resolving something invalid") {
      noon::uv_exception tExcept;
      REQUIRE_FALSE(tExcept);
      tUdp.resolve("this-doesnt-fing-exist")
        ->error([&](auto e) {
          tExcept = e;
          return nullptr;
        });
      tLoop.run();
      THEN("it returns an error") {
        // IPv4 or IPv6
        REQUIRE(tExcept);
      }
    }
  }
}
