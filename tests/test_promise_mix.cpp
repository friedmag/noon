#include "test_promise_common.hpp"

SCENARIO("Promise MIX test", "[promise]") {

  GIVEN("A mixture of promises") {
    WHEN("an IntPromise returns a VoidPromise") {
      int tVal = 0;
      using P = noon::basic_promise<int>;

      {
        P::create([&](P::Resolve aResolve, P::Error) {
            aResolve(19);
          })
          ->then<>([&](int aVal) {
            REQUIRE(aVal == 19);
            REQUIRE(tVal == 0);
            using P = noon::basic_promise<>;
            return P::create([&](P::Resolve aResolve, P::Error) {
                tVal = 42;
                aResolve();
            });
          })
          ->then([&]() {
              REQUIRE(tVal == 42);
              tVal = 27;
              return nullptr;
          });
      }
      REQUIRE(tVal == 27);
    }
  }
}
