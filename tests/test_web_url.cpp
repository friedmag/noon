#include <noon-web.hpp>
#include <catch.hpp>

SCENARIO("URL test", "[web url]") {
  GIVEN("A client") {
    noon::Loop tLoop;
    noon::web_client tClient(tLoop);
    WHEN("connecting with invalid URL") {
      REQUIRE_THROWS(tClient.connect("xxx://invalid"));
    }
    WHEN("connecting via TCP (needs to fail to connect)") {
      auto tConnect = tClient.connect("http://invalid:12345/");
      REQUIRE(tConnect);
      tLoop.run();
    }
    WHEN("connecting via UNIX socket (needs to fail to connect)") {
      auto tConnect = tClient.connect("file:///tmp/not/real/socket:/");
      REQUIRE(tConnect);
      tLoop.run();
    }
  }
}
