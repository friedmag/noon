#include <noon-uv-pipe.hpp>
#include <noon-uv-loop.hpp>
#include <catch.hpp>

SCENARIO("Pipe should have basic functionality", "[pipe]") {
  GIVEN("A loop with a pipe") {
    static std::string sPipeName = "/tmp/pipe_test";
    noon::Loop tLoop;
    noon::pipe tPipe(tLoop, false);
    WHEN("it is bound") {
      auto tStatus = tPipe.bind(sPipeName);
      THEN("it should not error out") {
        if (tStatus)
          FAIL("Failed in bind: " << tStatus.what());
      }
      AND_THEN("it should have an address") {
        REQUIRE(tPipe.address() == sPipeName);
      }
      AND_WHEN("it is listening") {
        std::unique_ptr<noon::pipe> tAccept;
        tPipe.listen([&](auto& e) {
          if (e)
            FAIL("Failed in listen: " << tStatus.what());
          tAccept = tPipe.accept<noon::pipe>();
          tPipe.close();
        });
        AND_THEN("someone else tries to bind/listen") {
          // NOTE: This is different from TCP! Since bind() always
          // does an unlink and then bind (maybe this should change?),
          // this listen will actually work!
          noon::pipe tOtherPipe(tLoop, false);
          tStatus = tOtherPipe.bind(sPipeName);
          REQUIRE_FALSE(tStatus);
          tStatus = tOtherPipe.listen([&](auto& e) {
            if (e)
              FAIL("Failed in secondary listen: " << e.what());
          });
          THEN("it should succeed!") {
            REQUIRE_FALSE(tStatus);
          }
        }
        THEN("someone connects") {
          noon::pipe tOtherPipe(tLoop, false);
          tOtherPipe.connect(sPipeName)
            ->error([&](auto e) {
              FAIL("Failed in connect: " << e.what());
              return nullptr;
            });
          tLoop.run();
          REQUIRE(tAccept);
          AND_THEN("someone sends a message") {
            std::string tMessage = "hello, pipe!";
            std::string tRead;
            tAccept->read([&](auto& e, auto aBuf, auto) {
              if (e)
                FAIL("Failed to read: " << e.what());
              tRead = aBuf->data;
              tAccept->readStop();
            });
            tOtherPipe.write(tMessage, [&](auto& e) {
              if (e)
                FAIL("Failed to write: " << e.what());
            });
            while (tLoop.run());
            // TODO: Catch should also allow text on REQUIRE, to verbalize expectations and allow printing on error of things not directly in the expression (ie, exception message for asynchronous ops (don't throw directly))
            REQUIRE(tRead.size() == tMessage.size());
            REQUIRE(tRead == tMessage);
            THEN("it goes the other way, too!") {
              tMessage = "hello to you as well!";
              tRead = "";
              REQUIRE(tRead.empty());
              tOtherPipe.read([&](auto& e, auto aBuf, auto) {
                if (e)
                  FAIL("Failed to read: " << e.what());
                tRead = aBuf->data;
                tOtherPipe.readStop();
              });
              tAccept->write(tMessage, [&](auto& e) {
                if (e)
                  FAIL("Failed to write: " << e.what());
              });
              while (tLoop.run());
              REQUIRE(tRead.size() == tMessage.size());
              REQUIRE(tRead == tMessage);
            }
          }

          AND_THEN("peer is set") {
            auto tPeer = tOtherPipe.peer();
            REQUIRE(tPeer != "");
          }
        }
      }
    }
  }
}
