#include <noon-uv-fs.hpp>
#include <noon-uv-loop.hpp>
#include <catch.hpp>

SCENARIO("Files should have basic functionality", "[file]") {
  static std::string sFileName = "file_test";
  noon::Loop tLoop;
  noon::file tFile(tLoop);
  bool tOpen = false;
  bool tWrote = false;
  bool tClose = false;

  GIVEN("A loop with a callback-based filesystem") {
    THEN("I can create, write, and close file") {
      tFile.open(sFileName, [&](auto) {
        tOpen = tFile.is_open();
        tFile.write("mydata", [&](auto) {
          tWrote = true;
          tFile.close([&](auto) {
            tClose = true;
          });
        });
      });
      tLoop.run();
      REQUIRE(tOpen);
      REQUIRE(tWrote);
      REQUIRE(tClose);
    }
  }

  GIVEN("A loop with a promise-based filesystem") {
    THEN("I can create, write, and close file") {
      auto test = tFile.open(sFileName)
        ->then([&](auto) {
          tOpen = tFile.is_open();
          return tFile.write("mydata");
        })
        ->then([&](auto) {
          tWrote = true;
          return tFile.close();
        })
        ->then<>([&](auto) {
          tClose = true;
          return nullptr;
        });
      tLoop.run();
      REQUIRE(tOpen);
      REQUIRE(tWrote);
      REQUIRE(tClose);
    }
  }
}
