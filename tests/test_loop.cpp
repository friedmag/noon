#include <thread>
#include <noon-uv-loop.hpp>
#include <catch.hpp>
#include <iostream>

using namespace std::literals::chrono_literals;

SCENARIO("Loop test", "[loop]") {
  noon::condvar tCond;
  noon::mutex tMutex;
  GIVEN("A loop") {
    noon::Loop tLoop;
    noon::lock tLock(tMutex);
    bool tRunning = true;
    WHEN("destroyed") {
      REQUIRE(true);
      // TODO
    }
    WHEN("exits") {
      REQUIRE(tRunning);
      std::thread([&] () {
        tLoop.run();
        noon::lock tLock(tMutex);
        tRunning = false;
        tCond.notify_all();
      }).detach();
      tCond.wait(tLock, [&]() { return !tRunning; });
      REQUIRE(!tRunning);
    }
    WHEN("exits with idle timer") {
      std::thread([&] () {
        tLoop.run();
        noon::lock tLock(tMutex);
        tRunning = false;
        tCond.notify_all();
      }).detach();

      noon::Timer timer(tLoop);

      tCond.wait(tLock);
      REQUIRE(!tRunning);
    }
    WHEN("loop won't exit with timer started") {
      std::thread([&] () {
        tLoop.run();
        printf("%f run exited\n", noon::now()); fflush(stdout);
        noon::lock tLock(tMutex);
        printf("%f lock acquired\n", noon::now()); fflush(stdout);
        tRunning = false;
        tCond.notify_all();
      }).detach();

      noon::Timer timer(tLoop);
      timer.start(200ms, 0s, [] () {
        printf("%f timer run\n", noon::now()); fflush(stdout);
              });

      REQUIRE(tRunning);
      printf("%f first wait\n", noon::now()); fflush(stdout);
      REQUIRE(tCond.wait_for(tLock, 100ms) == std::cv_status::timeout);
      printf("%f second wait\n", noon::now()); fflush(stdout);
      REQUIRE(tCond.wait_for(tLock, 200ms) == std::cv_status::no_timeout);
      REQUIRE(!tRunning);
    }
    WHEN("next") {
      int x = 0;
      tLoop.next([&] () {
        ++x;
      });
      REQUIRE(x == 0);
      tLoop.run(noon::Loop::Run::ONCE);
      REQUIRE(x == 1);
    }
    WHEN("exec") {
      int x = 0;
      tLoop.exec([&] () {
        ++x;
      });
      REQUIRE(x == 1);
    }

    WHEN("exec threaded") {
      int x = 0, y = 0;

      tLoop.next([&] () {
        noon::lock tLock(tMutex);
        ++x;
        tCond.notify_all();
      });

      tLoop.idle([&] (size_t id) {
        noon::lock tLock(tMutex);
        if (y > 0) {
          tLoop.cancel(id);
        }
      });

      std::thread([&] () {
        tLoop.run();
      }).detach();

      tCond.wait(tLock);
      REQUIRE(x == 1);

      tLoop.exec([&] () {
        noon::lock tLock(tMutex);
        ++y;
        tCond.notify_all();
      });

      tCond.wait(tLock);
      REQUIRE(x == 1);
      REQUIRE(y == 1);
    }

    WHEN("cancel called externally") {
      int x = 0;
      size_t tId;

      tLoop.idle([&] (size_t aId) {
        noon::lock tLock(tMutex);
        if (x == 0) {
          tId = aId;
          ++x;
          tCond.notify_all();
          tLoop.cancel(tId);
        }
      });

      auto tThread = std::thread([&] () {
        tLoop.run();
      });

      tCond.wait(tLock);
      REQUIRE(x == 1);
      tThread.join();
    }
  }
}
