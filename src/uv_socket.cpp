#include "noon-uv-socket.hpp"
#include "noon-uv-loop.hpp"
#include <iostream>
#include <cstring>
#include <errno.h>

namespace noon {

/////////////////////////////////////////////////////////////////////
// SOCKET FUNCTIONS
/////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& os, const socket::Address& addr)
{ return os << addr.addr << ':' << addr.port; };

socket::socket(Loop& aLoop)
  :mLoop(aLoop)
{
}

namespace {

struct getaddrinfo_data {
  uv_getaddrinfo_t mReq;
  ResolvePromise::Resolve mResolve;
  ResolvePromise::Error mError;

  getaddrinfo_data(auto aResolve, auto aError)
    :mResolve(aResolve), mError(aError) {};
};

void getaddrinfo_handler(uv_getaddrinfo_t* handle,
    int status, struct addrinfo* res)
{
  std::unique_ptr<getaddrinfo_data> tData(reinterpret_cast<getaddrinfo_data*>(handle));
  if (status < 0) {
      tData->mError(uv_exception(status));
  } else {
      sockaddr_in* sin = reinterpret_cast<sockaddr_in*>(res->ai_addr);
      char ret[16];
      if (inet_ntop(sin->sin_family, &sin->sin_addr.s_addr, ret, 16) == NULL) {
        tData->mError(uv_exception(errno));
      } else {
        tData->mResolve(ret);
      }
      uv_freeaddrinfo(res);
  }
}

};

ResolvePromise::PtrWrap socket::resolve(const std::string& aName)
{
  // TODO break resolve out into its own class (not strictly for sockets)
  return ResolvePromise::create([&](auto aResolve, auto aError) {
      auto tReq = new getaddrinfo_data(aResolve, aError);

      uv_getaddrinfo(mLoop.loop(), &tReq->mReq,
              getaddrinfo_handler, aName.c_str(), NULL, NULL);
    });
}

/////////////////////////////////////////////////////////////////////
// TCP FUNCTIONS
/////////////////////////////////////////////////////////////////////
tcp::tcp(Loop& aLoop)
  :socket(aLoop),
   stream(aLoop, reinterpret_cast<uv_stream_t*>(new uv_tcp_t)),
   mTcp(reinterpret_cast<uv_tcp_t*>(mStream.get()))
{
  uv_tcp_init(aLoop.loop(), mTcp.get());
}

uv_exception tcp::bind(const Address& aAddr)
{
  struct sockaddr_in tAddr;
  // TODO: Should deal with hostnames!
  uv_ip4_addr(aAddr.addr.c_str(), aAddr.port, &tAddr);

  return uv_tcp_bind(mTcp.get(),
          reinterpret_cast<struct sockaddr*>(&tAddr), 0);
}

namespace {

struct connect_data {
  uv_connect_t mReq;
  ConnectPromise::Resolve mResolve;
  ConnectPromise::Error mError;
};

void connect_handler(uv_connect_t* conn, int status)
{
  std::unique_ptr<connect_data> tReq(reinterpret_cast<connect_data*>(conn));
  if (status < 0) {
    tReq->mError(status);
  } else {
    tReq->mResolve();
  }
}

};

ConnectPromise::PtrWrap tcp::connect(const Address& aAddress)
{
  return ConnectPromise::create([=](auto aResolve, auto aError) {
      auto tPort = aAddress.port;
      this->resolve(aAddress.addr)
        ->then([this, tPort, aResolve, aError](auto aIP) {
          // TODO: IPv6 support?
          sockaddr_in tAddr;
          uv_ip4_addr(aIP.c_str(), tPort, &tAddr);
          auto tConn = new connect_data;
          int tStatus = uv_tcp_connect(&tConn->mReq,
              mTcp.get(), reinterpret_cast<struct sockaddr*>(&tAddr), connect_handler);
          if (tStatus < 0) {
            aError(tStatus);
            delete tConn;
          } else {
            tConn->mResolve = aResolve;
            tConn->mError = aError;
          }
          return ResolvePromise::Ptr();
        })
        ->error([aError](auto e) {
          aError(e);
          return ResolvePromise::Ptr();
        });
    });
}

socket::Address tcp::address()
{
  // TODO ipv4/ipv6 support?
  struct sockaddr_in tSin;
  auto tSaddr = reinterpret_cast<struct sockaddr*>(&tSin);
  int tSize = sizeof(tSin);
  uv_tcp_getsockname(mTcp.get(), tSaddr, &tSize);
  return Address(tSaddr);
}

socket::Address tcp::peer()
{
  // TODO ipv4/ipv6 support?
  struct sockaddr_in tSin;
  auto tSaddr = reinterpret_cast<struct sockaddr*>(&tSin);
  int tSize = sizeof(tSin);
  uv_tcp_getpeername(mTcp.get(), tSaddr, &tSize);
  return Address(tSaddr);
}

/////////////////////////////////////////////////////////////////////
// UDP FUNCTIONS
/////////////////////////////////////////////////////////////////////
namespace {

// common base for all callback types
// TODO: needed?
struct udp_data {
  socket::Address                 mTarget;
  ReadCallback                    mReadCb;
  FullReadCallback                mFullReadCb;
  CloseCallback                   mCloseCb;
  std::unique_ptr<buffer_manager> mBufferManager;

  udp_data() : mBufferManager(std::make_unique<buffer_manager>()) {};
};

};

#define UDP_DATA() reinterpret_cast<udp_data*>(mUdp->data)

udp::udp(Loop& aLoop)
  :socket(aLoop),
   mUdp(new uv_udp_t)
{
  uv_udp_init(aLoop.loop(), mUdp.get());
  mUdp->data = new udp_data;
}

udp::~udp()
{
  // TODO: This is kinda the same as Stream, but almost nothing else in Udp is, so I'm not inheriting... worth being smarter?
  readStop();
  close();
  mLoop.run(Loop::Run::ONCE);
}

uv_exception udp::bind(const Address& aAddress)
{
  struct sockaddr_in addr;
  // TODO: deal with resolve
  uv_ip4_addr(aAddress.addr.c_str(), aAddress.port, &addr);
  return uv_udp_bind(mUdp.get(),
          reinterpret_cast<struct sockaddr*>(&addr), 0);
}

void udp::broadcast(const Address& bcastAddr)
{
  uv_udp_set_broadcast(mUdp.get(), 1);
  target(bcastAddr);
}

void udp::target(const Address& addr)
{
  UDP_DATA()->mTarget = addr;
}

namespace {

void udp_read_handler(uv_udp_t* stream, ssize_t read, const uv_buf_t* buf,
    const struct sockaddr* addr, unsigned /*flags*/)
{
  auto tData = reinterpret_cast<udp_data*>(stream->data);
  if (read < 0) {
    delete tData->mBufferManager->free_buf(buf, 0);
    if (tData->mFullReadCb) tData->mFullReadCb(read, NULL, socket::Address(addr));
    else if (tData->mReadCb) tData->mReadCb(read, NULL, false);
  } else if (addr != NULL) {
    // Ignore NULL addr
    auto tBuf = tData->mBufferManager->free_buf(buf, read);
    if (tData->mFullReadCb) tData->mFullReadCb(0, buffer_ptr(tBuf), addr);
    else if (tData->mReadCb) tData->mReadCb(0, buffer_ptr(tBuf), false);
  } else {
    delete tData->mBufferManager->free_buf(buf, 0);
  }
}

void alloc_handler(uv_handle_t* handle, size_t size, uv_buf_t* buf)
{
  auto tData = reinterpret_cast<udp_data*>(handle->data);
  tData->mBufferManager->allocate_buf(buf, size);
}

};

void udp::read(ReadCallback cb)
{
  UDP_DATA()->mReadCb = cb;
  uv_udp_recv_start(mUdp.get(), alloc_handler, udp_read_handler);
}

void udp::readFull(FullReadCallback cb)
{
  UDP_DATA()->mFullReadCb = cb;
  uv_udp_recv_start(mUdp.get(), alloc_handler, udp_read_handler);
}

void udp::readStop()
{
  uv_udp_recv_stop(mUdp.get());
}

namespace {

struct UdpWriteRequest {
  uv_udp_send_t      req;
  uv_buf_t           buf;
  const std::string* str;
  WriteCallback      cb;
  udp*               raw;

  UdpWriteRequest(udp* raw_, WriteCallback& cb_) : str(NULL), cb(cb_), raw(raw_) {};
};

inline void udp_write_free(UdpWriteRequest* req) {
  if (req->str != NULL) {
    delete req->str;
  } else {
    delete [] req->buf.base;
  }
  delete req;
}

void udp_write_handler(uv_udp_send_t* req_, int status) {
  UdpWriteRequest* req = (UdpWriteRequest*)req_;
  if (req->cb) req->cb(status);
  udp_write_free(req);
}

};

void udp::write(const std::string* str, WriteCallback cb)
{
  UdpWriteRequest* req = new UdpWriteRequest(this, cb);
  req->str = str;
  req->buf = uv_buf_init(const_cast<char*>(str->data()), str->size());
  struct sockaddr_in send_addr;
  UDP_DATA()->mTarget.ip4(send_addr);
  int status = uv_udp_send((uv_udp_send_t*)req, mUdp.get(), &req->buf, 1,
      (struct sockaddr*)&send_addr, udp_write_handler);
  if (status < 0) {
    cb(status);
    udp_write_free(req);
  }
}

void udp::write(const std::string& str, WriteCallback cb)
{
  write(str.data(), str.size(), cb);
}

void udp::write(const void* buf, size_t len, WriteCallback cb)
{
  UdpWriteRequest* req = new UdpWriteRequest(this, cb);
  req->buf = uv_buf_init(new char[len], len);
  memcpy(req->buf.base, buf, len);
  struct sockaddr_in send_addr;
  UDP_DATA()->mTarget.ip4(send_addr);
  int status = uv_udp_send((uv_udp_send_t*)req, mUdp.get(), &req->buf, 1,
      (struct sockaddr*)&send_addr, udp_write_handler);
  if (status < 0) {
    cb(status);
    udp_write_free(req);
  }
}

static void udp_close_handler(uv_handle_t* handle)
{
  auto tData = reinterpret_cast<udp_data*>(handle->data);
  if (tData->mCloseCb) tData->mCloseCb();
  delete tData;
  handle->data = NULL;
}

void udp::close(CloseCallback cb)
{
  auto tHandle = reinterpret_cast<uv_handle_t*>(mUdp.get());
  if (!uv_is_closing(tHandle)) {
    UDP_DATA()->mCloseCb = cb;
    uv_close(tHandle, udp_close_handler);
  }
}

socket::Address udp::address()
{
  // TODO ipv4/ipv6 support?
  struct sockaddr_in sin;
  auto tSaddr = reinterpret_cast<struct sockaddr*>(&sin);
  int size = sizeof(sin);
  uv_udp_getsockname(mUdp.get(), tSaddr, &size);
  return Address(tSaddr);
}

socket::Address udp::peer()
{
  return UDP_DATA()->mTarget;
}

};
