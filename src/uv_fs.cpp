#include "noon-uv-fs.hpp"
#include "noon-uv-loop.hpp"
#include <iostream>

namespace {

struct ReqWrapper {
  noon::file*          file;
  std::string          data;
  noon::file::Callback cb;

  ReqWrapper(noon::file* file_, noon::file::Callback cb_)
    :file(file_), cb(cb_) {};
  ReqWrapper(noon::file* file_, const std::string& data_,
      noon::file::Callback cb_)
    :file(file_), data(data_), cb(cb_) {};
};

void file_handler(uv_fs_t* req)
{
  auto wrapper = reinterpret_cast<ReqWrapper*>(req->data);
  if (wrapper->cb) wrapper->cb(req);
  delete wrapper;
  delete req;
}

void open_file_handler(uv_fs_t* req)
{
  auto wrapper = reinterpret_cast<ReqWrapper*>(req->data);
  wrapper->file->fd(req->result);
  uv_fs_req_cleanup(req);
  file_handler(req);
}

void stat_file_handler(uv_fs_t* req)
{
  auto wrapper = reinterpret_cast<ReqWrapper*>(req->data);
  wrapper->file->offset(req->statbuf.st_size);
  file_handler(req);
}

};

noon::file::file(noon::Loop& loop)
  :loop_(loop), fd_(0), offset_(-1)
{
}

void noon::file::open(const std::string& path,
    int flags, int mode, Callback cb)
{
  auto file = new uv_fs_t;
  file->data = new ReqWrapper(this, cb);
  uv_fs_open(loop_.loop(), file, path.c_str(), flags, mode, open_file_handler);
}

void noon::file::write(const void* data, uint32_t size, int64_t offset, Callback cb)
{
  auto file = new uv_fs_t;
  auto wrapper = new ReqWrapper(this, std::string(reinterpret_cast<const char*>(data), size), cb);
  file->data = wrapper;
  uv_buf_t buf = uv_buf_init(const_cast<char*>(wrapper->data.data()), size);
  uv_fs_write(loop_.loop(), file, fd_, &buf, 1, offset, file_handler);
  if (offset_ > -1) offset_ += size;
}

void noon::file::stat(Callback cb)
{
  auto file = new uv_fs_t;
  file->data = new ReqWrapper(this, cb);
  uv_fs_fstat(loop_.loop(), file, fd_, stat_file_handler);
}

void noon::file::fsync(Callback cb)
{
  if (cb) {
    auto file = new uv_fs_t;
    file->data = new ReqWrapper(this, cb);
    uv_fs_fsync(loop_.loop(), file, fd_, file_handler);
  } else {
    uv_fs_t req;
    uv_fs_fsync(loop_.loop(), &req, fd_, NULL);
  }
}

void noon::file::close(Callback cb)
{
  if (cb) {
    auto file = new uv_fs_t;
    file->data = new ReqWrapper(this, cb);
    uv_fs_close(loop_.loop(), file, fd_, file_handler);
  } else {
    uv_fs_t req;
    uv_fs_close(loop_.loop(), &req, fd_, NULL);
  }
  fd_ = 0;
}
