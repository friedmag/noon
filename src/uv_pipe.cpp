#include "noon-uv-pipe.hpp"
#include "noon-uv-loop.hpp"
#include <unistd.h>

namespace noon {

namespace {

struct data {
    virtual ~data() {};
};

};

pipe::pipe(Loop& aLoop, bool aIPC)
  :stream(aLoop, reinterpret_cast<uv_stream_t*>(new uv_pipe_t)),
   mPipe(reinterpret_cast<uv_pipe_t*>(mStream.get()))
{
  // Allocate new pipe wrapper, initialize
  uv_pipe_init(loop().loop(), mPipe.get(), aIPC ? 1 : 0);
}

uv_exception pipe::bind(const std::string& aName)
{
  // TODO: Can't exactly asynchronously unlink and then synchronously bind, so...
  unlink(aName.c_str());
  return uv_pipe_bind(mPipe.get(), aName.c_str());
}

namespace {

struct connect_data {
  uv_connect_t mReq;
  ConnectPromise::Resolve mResolve;
  ConnectPromise::Error mError;

  connect_data(auto aResolve, auto aError)
    :mResolve(aResolve), mError(aError) {};
};

void connect_handler(uv_connect_t* conn, int status)
{
  std::unique_ptr<connect_data> tData(reinterpret_cast<connect_data*>(conn));
  if (status < 0) {
    tData->mError(status);
  } else {
    tData->mResolve();
  }
}

};

ConnectPromise::PtrWrap pipe::connect(const std::string& aName)
{
  return ConnectPromise::create([&](auto aResolve, auto aError) {
      auto tData = new connect_data(aResolve, aError);
      uv_pipe_connect(&tData->mReq, mPipe.get(), aName.c_str(),
              connect_handler);
    });
}

std::string pipe::address() const
{
    std::string tStr;
    size_t tSize = 100;
    tStr.resize(tSize);
    uv_pipe_getsockname(mPipe.get(),
            const_cast<char*>(tStr.c_str()), &tSize);
    tStr.resize(tSize);
    return tStr;
}

std::string pipe::peer() const
{
    std::string tStr;
    size_t tSize = 100;
    tStr.reserve(tSize);
    uv_pipe_getpeername(mPipe.get(),
            const_cast<char*>(tStr.c_str()), &tSize);
    tStr.resize(tSize);
    return tStr;
}

};
