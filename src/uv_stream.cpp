#include "noon-uv-stream.hpp"
#include "noon-uv-loop.hpp"
#include <iostream>
#include <cstring>
#include <errno.h>

namespace noon {

namespace {

enum class data_e {
  invalid,
  listen,
  read,
  new_read,
  close
};

// TODO: Should probably put all necessary data in the one struct, rather than inherit
struct data {
  data(data_e type_) :type(type_) {};
  virtual ~data() {};
  data_e type;
};

};

stream::stream(Loop& aLoop, uv_stream_t* aStream)
  :mLoop(&aLoop),
   mStream(aStream),
   mBufferManager(new buffer_manager)
{
  mStream->data = nullptr;
}

stream::~stream()
{
  // TODO: Is this enough cleanup?  What about other pending ops?
  readStop();
  close();
  mLoop->run(Loop::Run::ONCE);
}

namespace {

struct listen_data : public data {
  listen_data() : data(data_e::listen) {};
  ListenCallback mCb;
};

void listen_handler(uv_stream_t* stream, int status)
{
  listen_data* tData = reinterpret_cast<listen_data*>(stream->data);
  tData->mCb(uv_exception(status));
}

};

uv_exception stream::listen(ListenCallback aCb)
{
  listen_data* tData = new listen_data;
  tData->mCb = aCb;
  mStream->data = tData;
  // TODO: Should this call the callback directly instead of return?
  // I think I've been inconsistent at this point.
  return uv_listen(mStream.get(), 511, listen_handler);
}

/////////////////////////////////////////////////////////////////////
// WRITE FUNCTIONS
/////////////////////////////////////////////////////////////////////
namespace {

struct StreamWriteRequest;
using Allocator = std::function<void (StreamWriteRequest*)>;

struct StreamWriteRequest {
    uv_write_t                 mReq;
    uv_buf_t                   mBuf;
    std::unique_ptr<data_type> mStr;
    bool                       mDel;
    WriteCallback              mCb;

    StreamWriteRequest(WriteCallback& aCb)
        :mDel(false), mCb(aCb)
    {};
    ~StreamWriteRequest()
    {
        if (mDel) {
            delete [] mBuf.base;
        }
    };
};

void stream_write_handler(uv_write_t* aReq, int aStatus) {
    StreamWriteRequest* tReq = reinterpret_cast<StreamWriteRequest*>(aReq);
    if (tReq->mCb)
        tReq->mCb(aStatus);
    delete tReq;
}

void stream_write(uv_stream_t* aStream, Allocator aAlloc, WriteCallback cb)
{
  StreamWriteRequest* tReq = new StreamWriteRequest(cb);
  aAlloc(tReq);
  int tStatus = uv_write(reinterpret_cast<uv_write_t*>(tReq),
          aStream, &tReq->mBuf, 1, stream_write_handler);
  if (tStatus < 0) {
    // TODO: Deallocator? for instance, str* is assumed to be deleted on this call, but it won't be? Could be responsible up-stream I guess...
    cb(uv_exception(tStatus));
    delete tReq;
  }
}

};

void stream::write(const data_type* str, WriteCallback cb)
{
    stream_write(mStream.get(),
        [&](auto aReq) {
            aReq->mBuf = uv_buf_init(const_cast<char*>(str->data()), str->size());
        }, cb);
}

void stream::write(std::unique_ptr<data_type> str, WriteCallback cb)
{
    stream_write(mStream.get(),
        [&](auto aReq) {
            aReq->mBuf = uv_buf_init(const_cast<char*>(str->data()), str->size());
            aReq->mStr = std::move(str);
        }, cb);
}

void stream::write(const data_type& str, WriteCallback cb)
{
  write(str.data(), str.size(), cb);
}

void stream::write(const void* buf, size_t len, WriteCallback cb)
{
    stream_write(mStream.get(),
        [=](auto aReq) {
            aReq->mDel = true;
            aReq->mBuf = uv_buf_init(new char[len], len);
            memcpy(aReq->mBuf.base, buf, len);
        }, cb);
}

/////////////////////////////////////////////////////////////////////
// READ FUNCTIONS
/////////////////////////////////////////////////////////////////////
namespace {

struct new_read_data : public data {
    data_type& mBuf;
    size_t mOffset;
    NewReadCallback mCb;
    new_read_data(data_type& aBuf)
        :data(data_e::new_read), mBuf(aBuf), mOffset(0) {};
};

void new_alloc_handler(uv_handle_t* handle, size_t, uv_buf_t* buf)
{
  auto tData = static_cast<new_read_data*>(handle->data);
  buf->base = &tData->mBuf[tData->mOffset];
  buf->len = tData->mBuf.size() - tData->mOffset;
}

void new_stream_read_handler(uv_stream_t* stream, ssize_t read, const uv_buf_t*)
{
  auto tData = reinterpret_cast<new_read_data*>(stream->data);
  if (tData->type != data_e::new_read) {
    std::cout << "Incorrect type in new_stream_read_handler - aborting" << std::endl;
    return;
  }
  if (read < 0) {
    tData->mCb(uv_exception(read));
  } else {
    tData->mOffset += read;
    if (tData->mOffset == tData->mBuf.size()) {
        uv_read_stop(stream);
        stream->data = nullptr;
        tData->mCb(false);
        delete tData;
    } else {
    }
  }
}

struct read_data : public data {
  read_data() : data(data_e::read) {};
  ReadCallback    mCb;
  buffer_manager* mBufferManager;
};

void alloc_handler(uv_handle_t* handle, size_t size, uv_buf_t* buf)
{
  read_data* tData = reinterpret_cast<read_data*>(handle->data);
  tData->mBufferManager->allocate_buf(buf, size);
}

void stream_read_handler(uv_stream_t* stream, ssize_t read, const uv_buf_t* buf)
{
  read_data* tData = reinterpret_cast<read_data*>(stream->data);
  if (!tData) return;
  if (tData->type != data_e::read) {
    std::cout << "Incorrect type in stream_read_handler - aborting" << std::endl;
    return;
  }
  if (read < 0) {
    delete tData->mBufferManager->free_buf(buf, 0);
    if (read == UV_EOF) {
      tData->mCb(0, nullptr, true);
    } else {
      tData->mCb(uv_exception(read), nullptr, false);
    }
  } else {
    tData->mCb(uv_exception(read),
            buffer_ptr(tData->mBufferManager->free_buf(buf, read)), false);
  }
}

};

ReadPromise::Ptr stream::read(data_type& buf)
{
  return std::make_shared<ReadPromise>(
    [&](auto aResolve, auto aError) {
      this->read(buf, [=](CException e) {
        if (e) {
          aError(e);
        }
        aResolve();
      });
  });
}

void stream::read(data_type& aBuf, NewReadCallback aCb)
{
  auto tData = new new_read_data(aBuf);
  mStream->data = tData;
  tData->mCb = aCb;
  uv_read_start(mStream.get(), new_alloc_handler, new_stream_read_handler);
}

void stream::read(ReadCallback aCb)
{
  auto tData = new read_data;
  tData->mCb = aCb;
  tData->mBufferManager = mBufferManager.get();
  mStream->data = tData;
  uv_read_start(mStream.get(), alloc_handler, stream_read_handler);
}

void stream::readStop()
{
  delete reinterpret_cast<data*>(mStream->data);
  mStream->data = nullptr;
  uv_read_stop(mStream.get());
}

/////////////////////////////////////////////////////////////////////
// CLOSE FUNCTIONS
/////////////////////////////////////////////////////////////////////
namespace {

struct close_data : public data {
  close_data() : data(data_e::close) {};
  CloseCallback mCb;
};

void stream_close_handler(uv_handle_t* handle)
{
  auto tData = reinterpret_cast<close_data*>(handle->data);
  if (!tData) return;
  if (tData->type != data_e::close) {
    std::cout << "Incorrect type in stream_close_handler - aborting" << std::endl;
    return;
  }
  if (tData->mCb) tData->mCb();
  delete tData;
}

};

void stream::close(CloseCallback aCb)
{
  auto tHandle = reinterpret_cast<uv_handle_t*>(mStream.get());
  if (!uv_is_closing(tHandle)) {
      close_data* tData = nullptr;
      if (aCb) {
          tData = new close_data;
          tData->mCb = aCb;
      }
      delete reinterpret_cast<data*>(mStream->data);
      mStream->data = tData;
      // TODO: Need to clean memory in callback... deleting ahead
      // of time like this could crash if read were in progress,
      // since libuv will call their callbacks w/ errors, which
      // would access the data I just deleted!
      uv_close(tHandle, stream_close_handler);
  }
}

};
