#include "noon-uv-loop.hpp"
#include <iostream>
#include <thread>
#include <vector>
#include <cstring>
#include <errno.h>

using namespace std::literals::chrono_literals;

namespace noon {

struct AsyncRequest {
    uv_async_t req;
    Loop*  loop;

    AsyncRequest(Loop* loop_) :loop(loop_) {};
    ~AsyncRequest() {
        uv_close(reinterpret_cast<uv_handle_t*>(&req), NULL);
        loop->run(Loop::Run::ONCE);
    }
};

struct Loop::Internal {

  using handler_key = size_t;
  using handler_close = std::function<void(close_cb)>;
  using handler_wrap = std::function<handler_close(handler_key)>;

    bool ownLoop_;
    uv_loop_t*      loop_;
    Timer*          timer_;
    std::thread::id thread_;

    noon::mutex               asyncLock_;
    std::vector<NextCallback> asyncCallbacks_;
    AsyncRequest*             async_;
    std::vector<handler_wrap> handlers_;
    std::vector<handler_close> handlerClosers_;

    ~Internal() {
        delete async_;
        delete timer_;
        if (ownLoop_) {
            while (uv_loop_close(loop_) < 0) {
                uv_run(loop_, UV_RUN_NOWAIT);
            }
            delete loop_;
        }
    }

    void add(handler_wrap aFunc) {
      auto tPos = handlers_.size();
      handlers_.push_back(aFunc);
      handlerClosers_.push_back(handlers_[tPos](tPos));
    };

    void cancel(handler_key aKey) {
      handlerClosers_[aKey]([=]() {
        handlerClosers_[aKey] = handler_close();
        handlers_[aKey] = handler_wrap();
      });
    };
};

namespace {

  void async_cb(AsyncRequest* async)
  {
    async->loop->callAsync();
  }

};

Loop::Loop(uv_loop_t* loop)
  :internal_(new Internal)
{
  internal_->async_ = new AsyncRequest(this);
  internal_->ownLoop_ = (loop == NULL);
  internal_->loop_ = internal_->ownLoop_ ? new uv_loop_t : loop;
  if (internal_->ownLoop_) uv_loop_init(internal_->loop_);
  internal_->timer_ = new Timer(*this, false);
  internal_->loop_->data = this;
  internal_->thread_ = std::this_thread::get_id();
  uv_async_init(internal_->loop_, reinterpret_cast<uv_async_t*>(internal_->async_), reinterpret_cast<uv_async_cb>(async_cb));
  uv_unref(reinterpret_cast<uv_handle_t*>(internal_->async_));
  signal(SIGPIPE, SIG_IGN);
}

Loop::~Loop()
{
}

uv_loop_t* Loop::loop() { return internal_->loop_; };
bool Loop::currentThread() const
{ return std::this_thread::get_id() == internal_->thread_; };

bool Loop::run(Run flag)
{
  internal_->thread_ = std::this_thread::get_id();
  return (uv_run(internal_->loop_, static_cast<uv_run_mode>(flag)) != 0);
}

struct idle_t {
  using uv_type = uv_idle_t;
  uv_type uv = uv_type();
  size_t id = 0;
  Loop::idle_cb cb;
  Loop::close_cb close_cb;
};

template<typename T>
void misc_cb(typename T::uv_type* handle) {
  auto typed = reinterpret_cast<T*>(handle);
  typed->cb(typed->id);
}

template<typename T>
void close_handler(uv_handle_t* handle) {
  auto typed = reinterpret_cast<T*>(handle);
  typed->close_cb();
}

void Loop::idle(idle_cb cb)
{
  idle_t tIdle;
  auto tHandler = [=](idle_key id) mutable {
      uv_idle_init(internal_->loop_, &tIdle.uv);
      tIdle.cb = cb;
      tIdle.id = id;
      uv_idle_start(&tIdle.uv, misc_cb<idle_t>);
      return [&](close_cb cb) {
        tIdle.close_cb = cb;
        //uv_idle_stop(&tIdle.uv);
        uv_close(reinterpret_cast<uv_handle_t*>(&tIdle.uv), close_handler<idle_t>);
      };
  };
  if (currentThread()) {
    internal_->add(tHandler);
  } else {
    next([=]() {
      internal_->add(tHandler);
    });
  }
}

void Loop::cancel(idle_key key)
{
  if (currentThread()) {
    internal_->cancel(key);
  } else {
    exec([=]() {
      internal_->cancel(key);
    });
  }
}

void Loop::next(NextCallback cb)
{
  if (currentThread()) {
    internal_->timer_->start(0s, 0s, [=] () {
      cb();
    });
  } else {
    exec(cb);
  }
}

void Loop::exec(NextCallback cb)
{
  if (currentThread()) {
    cb();
  } else {
    // Non-loop thread - Use uv_async_send to perform call
    noon::lock guard(internal_->asyncLock_);
    internal_->asyncCallbacks_.push_back(cb);
    uv_async_send(reinterpret_cast<uv_async_t*>(internal_->async_));
  }
}

void Loop::callAsync()
{
  noon::lock guard(internal_->asyncLock_);
  for (auto cb : internal_->asyncCallbacks_) {
    cb();
  }
  internal_->asyncCallbacks_.clear();
}

/////////////////////////////////////////////////////////////////////
// TIMER FUNCTIONS
/////////////////////////////////////////////////////////////////////
namespace {

struct timer_data {
    Timer::TimerCallback mCb;
};

void timer_handler(uv_timer_t* timer)
{
  auto t = reinterpret_cast<timer_data*>(timer->data);
  t->mCb();
}

};

#define TIMER_DATA() reinterpret_cast<timer_data*>(timer_.data)

Timer::Timer(Loop& loop, bool unref)
  :loop_(&loop)
{
  uv_timer_init(loop.loop(), &timer_);
  if (unref) uv_unref(reinterpret_cast<uv_handle_t*>(&timer_));
  timer_.data = new timer_data;
}

Timer::~Timer()
{
    delete TIMER_DATA();
    uv_close(reinterpret_cast<uv_handle_t*>(&timer_), NULL);
    loop_->run(Loop::Run::ONCE);
}

void Timer::start(std::chrono::milliseconds timeout,
        std::chrono::milliseconds repeat, TimerCallback cb)
{
  TIMER_DATA()->mCb = cb;
  uv_timer_start(&timer_, timer_handler, timeout.count(), repeat.count());
}

void Timer::stop()
{
  uv_timer_stop(&timer_);
}

void Timer::again()
{
  uv_timer_again(&timer_);
}

void Timer::setRepeat(std::chrono::milliseconds repeat)
{
  uv_timer_set_repeat(&timer_, repeat.count());
}

int64_t Timer::getRepeat()
{
  return uv_timer_get_repeat(&timer_);
}

};
