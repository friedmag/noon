#!/bin/bash

lcov -c -d */tests/CMakeFiles/noon_tests.dir -d */src/CMakeFiles/noon.dir -o cov.info
genhtml cov.info -o out
