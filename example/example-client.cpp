#include "noon.hpp"

int main()
{
  noon::Loop tLoop;
  noon::web_client tClient(tLoop);

//  printf("GET localhost...\n");
//  tClient.get("http://localhost:2000/hello")->then<>([](auto) {
//    printf("GET response\n");
//    return nullptr;
//  });
//  printf("PUT localhost...\n");
//  tClient.put("http://localhost:2000/hello")->then<>([](auto) {
//    printf("PUT response\n");
//    return nullptr;
//  });
  printf("POST localhost...\n");
  tClient.post("http://localhost:2000/hello", "slap")->then<>([](auto) {
    printf("POST response\n");
    return nullptr;
  });
//  printf("DELETE localhost...\n");
//  tClient.remove("http://localhost:2000/hello")->then([](auto) {
//    printf("DELETE response\n");
//    return nullptr;
//  });

  tLoop.run();

  return 0;
}
