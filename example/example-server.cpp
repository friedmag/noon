#include "noon.hpp"

int main()
{
  static uint16_t sPort = 2000;

  noon::Loop tLoop;
  noon::web_server tServer(tLoop);

  tServer.listen(sPort);
  tServer.get("/hello", [&](auto, auto aRes) {
    printf("GET hello!\n");
    aRes->end("goodbye");
  });
  tServer.put("/hello", [&](auto, auto aRes) {
    printf("PUT hello!\n");
    aRes->end("putted");
  });
  tServer.post("/hello", [&](auto aReq, auto aRes) {
    printf("POST hello!\n");
    aReq->onData([=](auto aStr) {
        printf("READ DATA: %s\n", aStr.c_str());
      });
    aReq->onEnd()
      ->then([=]() {
        aRes->end("posted");
        return nullptr;
      });
  });
  tServer.remove("/hello", [&](auto, auto aRes) {
    printf("DELETE hello!\n");
    aRes->end("removed");
  });

  tLoop.run();

  return 0;
}
