// Copyright 2016
// Gary Friedman

#pragma once

#include <sstream>
#include <uv.h>

namespace noon {

class uv_exception : public std::exception {
public:
  uv_exception() {};
  uv_exception(const std::string& aStr) : mStr(aStr) {};
  uv_exception(int aCode) {
    if (aCode < 0) {
      std::stringstream tStream;
      tStream << "uv_exception: " << uv_err_name(aCode) << ": " << uv_strerror(aCode);
      mStr = tStream.str();
    }
  };

  virtual const char* what() const noexcept override {
    return mStr.c_str();
  };

  operator bool() const {
    return !mStr.empty();
  };

private:
  std::string mStr;
};

using CException = const uv_exception&;

};
