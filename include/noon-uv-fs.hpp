// Copyright 2016
// Gary Friedman

#pragma once

#include "noon-promise.hpp"
#include "noon-uv-common.hpp"
#include <iostream>

namespace noon
{

class Loop;

class file {
  public:
    using CResult = const uv_fs_t*;

    using Callback = std::function<void (CResult result)>;
    using Promise = promise<uv_exception, CResult>;

    file(Loop& loop);
 
    void open(const std::string& path, Callback cb)
    { open(path, O_CREAT | O_RDWR, 0664, cb); };
    void open(const std::string& path,
        int flags, int mode, Callback cb);
    auto open(const std::string& aPath)
    {
      return Promise::create([&](auto aResolve, auto aError) {
        this->open(aPath, [=](auto aRes) {
          if (aRes->result < 0) aError(uv_exception(aRes->result));
          else aResolve(aRes);
        });
      });
    };

    void write(const std::string& aStr, Callback cb)
    { write(aStr.data(), aStr.size(), cb); };
    void write(const void* data, uint32_t size, Callback cb)
    { write(data, size, offset_, cb); };
    void write(const void* data, uint32_t size, int64_t offset, Callback cb);
    auto write(const std::string& aStr)
    {
      return Promise::create([=](auto aResolve, auto aError) {
        this->write(aStr, [=](auto aRes) {
          if (aRes->result < 0) aError(uv_exception(aRes->result));
          else aResolve(aRes);
        });
      });
    };

    void stat(Callback cb);
    auto stat()
    {
      return Promise::create([=](auto aResolve, auto aError) {
        this->stat([=](auto aRes) {
          if (aRes->result < 0) aError(uv_exception(aRes->result));
          else aResolve(aRes);
        });
      });
    };

    void fsync(Callback cb);
    auto fsync()
    {
      return Promise::create([&](auto aResolve, auto aError) {
        this->fsync([=](auto aRes) {
          if (aRes->result < 0) aError(uv_exception(aRes->result));
          else aResolve(aRes);
        });
      });
    };

    void close(Callback cb);
    auto close()
    {
      return Promise::create([&](auto aResolve, auto aError) {
        this->close([=](auto aRes) {
          if (aRes->result < 0) aError(aRes->result);
          else aResolve(aRes);
        });
      });
    };

    bool is_open() { return fd_ != 0; };

    int fd() { return fd_; };
    void fd(int fd) { fd_ = fd; };

    int64_t offset() { return offset_; };
    void offset(int64_t offset) { offset_ = offset; };

  private:
    noon::Loop& loop_;
    int       fd_;
    int64_t   offset_;
};

class fs {
};

};
