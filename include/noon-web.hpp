// Copyright 2016
// Gary Friedman

#pragma once

#include <sstream>
#include <http_parser.h>
#include "noon-common.hpp"
#include "noon-uv-buffer.hpp"

namespace noon {

static const auto HTTP_STATUS_CODES = map<int, std::string>(
    100, "Continue",
    200, "OK",
    201, "Created",
    204, "No Content",
    301, "Moved Permanently",
    304, "Not Modified",
    400, "Bad Request",
    401, "Unauthorized",
    403, "Forbidden",
    404, "Not Found",
    500, "Internal Server Error"
  );
static const auto HTTP_DEFAULT_HEADERS = map<std::string, std::string>(
    "Connection", "close");

namespace {

template<typename T>
int on_message_begin(http_parser*);
template<typename T>
int on_url(http_parser*, const char*, size_t);
template<typename T>
int on_status(http_parser*, const char*, size_t);
template<typename T>
int on_header_field(http_parser*, const char*, size_t);
template<typename T>
int on_header_value(http_parser*, const char*, size_t);
template<typename T>
int on_headers_complete(http_parser*);
template<typename T>
int on_body(http_parser*, const char*, size_t);
template<typename T>
int on_message_complete(http_parser*);
template<typename T>
int on_chunk_header(http_parser*);
template<typename T>
int on_chunk_complete(http_parser*);

};

class web_client;
class web_server;
using header_map = std::map<std::string, std::string>;
using options = std::map<std::string, std::string>;

template<typename T>
struct web_parser {
  web_parser(T* data)
  {
    http_parser_init(&mParser, HTTP_BOTH);
    mParser.data = data;
    mParserSettings.on_message_begin = on_message_begin<T>;
    mParserSettings.on_url = on_url<T>;
    mParserSettings.on_status = on_status<T>;
    mParserSettings.on_header_field = on_header_field<T>;
    mParserSettings.on_header_value = on_header_value<T>;
    mParserSettings.on_headers_complete = on_headers_complete<T>;
    mParserSettings.on_body = on_body<T>;
    mParserSettings.on_message_complete = on_message_complete<T>;
    mParserSettings.on_chunk_header = on_chunk_header<T>;
    mParserSettings.on_chunk_complete = on_chunk_complete<T>;
  };

  auto execute()
  {
    /*auto tParsed =*/ http_parser_execute(&mParser, &mParserSettings,
        nullptr, 0);
  }

  auto execute(buffer* tBuf)
  {
    /*auto tParsed =*/ http_parser_execute(&mParser, &mParserSettings,
        tBuf->c_raw(), tBuf->size());
  }

  auto status_code()
  {
    return mParser.status_code;
  };

  http_parser mParser;
  http_parser_settings mParserSettings;
};

namespace {
template<typename T>
int on_message_begin(http_parser* aParser) {
  //printf("on_message_begin\n");
  return static_cast<T*>(aParser->data)->onMessageBegin();
};
template<typename T>
int on_url(http_parser* aParser, const char* aStr, size_t aLen) {
  //printf("on_url: (%lu) %s\n", aLen, aStr);
  return static_cast<T*>(aParser->data)->onUrl(std::string(aStr, aLen));
};
template<typename T>
int on_status(http_parser* aParser, const char* aStr, size_t aLen) {
  //printf("on_status: (%lu) %s\n", aLen, aStr);
  return static_cast<T*>(aParser->data)->onStatus(std::string(aStr, aLen));
};
template<typename T>
int on_header_field(http_parser* aParser, const char* aStr, size_t aLen) {
  //printf("on_header_field: (%lu) %s\n", aLen, aStr);
  return static_cast<T*>(aParser->data)->onHeaderField(std::string(aStr, aLen));
};
template<typename T>
int on_header_value(http_parser* aParser, const char* aStr, size_t aLen) {
  //printf("on_header_value: (%lu) %s\n", aLen, aStr);
  return static_cast<T*>(aParser->data)->onHeaderValue(std::string(aStr, aLen));
};
template<typename T>
int on_headers_complete(http_parser* aParser) {
  //printf("on_headers_complete\n");
  return static_cast<T*>(aParser->data)->onHeadersComplete();
};
template<typename T>
int on_body(http_parser* aParser, const char* aStr, size_t aLen) {
  //printf("on_body - %p (%lu) - %s\n", aStr, aLen, aStr);
  return static_cast<T*>(aParser->data)->onBody(std::string(aStr, aLen));
};
template<typename T>
int on_message_complete(http_parser* aParser) {
  //printf("on_message_complete\n");
  return static_cast<T*>(aParser->data)->onMessageComplete();
};
template<typename T>
int on_chunk_header(http_parser*) {
  //printf("on_chunk_header\n");
  return 0;//static_cast<T*>(aParser->data)->onChunkHeader();
};
template<typename T>
int on_chunk_complete(http_parser*) {
  //printf("on_chunk_complete\n");
  return 0;//static_cast<T*>(aParser->data)->onChunkComplete();
};
};

};

#include "noon-web-client.hpp"
#include "noon-web-server.hpp"
