// Copyright 2016
// Gary Friedman

#pragma once

#include "noon-uv-common.hpp"
#include <map>

namespace noon
{

struct buffer
{
  std::string data;
  size_t      offset;

  /** Default - does nothing */
  inline buffer() : offset(0) {};
  /** Copy - creates a copy! */
  inline buffer(const buffer& o)
    :data(o.data), offset(o.offset) {};
  /** Copy - takes ownership! */
  inline buffer(buffer& o)
    :offset(o.offset)
  { data.swap(o.data); };
  /** string - creates a copy! */
  inline buffer(const std::string& data_)
    :data(data_), offset(0) {};
  /** string - takes ownership! */
  inline buffer(std::string& data_)
    :offset(0)
  { data.swap(data_); };
  inline virtual ~buffer() {};

  inline const char* c_raw() const { return data.data() + offset; };
  inline char* raw() { return const_cast<char*>(data.data() + offset); };
  inline size_t size() const { return data.size() - offset; };
  inline bool forward(size_t offset) { this->offset += offset; return this->offset < data.size(); };
  inline void reset() { this->offset = 0; };
};

using buffer_ptr = std::unique_ptr<buffer>;

struct buffer_manager {

struct BufComp {
    bool operator()(const uv_buf_t& a, const uv_buf_t& b) const
    { return a.base < b.base; };
};

using BufferMap = std::map<uv_buf_t, buffer*, BufComp>;
BufferMap mBuffers;

inline void allocate_buf(uv_buf_t* buf_t, size_t size)
{
  buffer* buf = new buffer;
  buf->data.resize(size);
  buf_t->base = const_cast<char*>(buf->data.data());
  buf_t->len = size;
  mBuffers[*buf_t] = buf; // TODO use insert/emplace
}

inline buffer* free_buf(const uv_buf_t* buf_t, size_t size) {
  auto i = mBuffers.find(*buf_t);
  if (i == mBuffers.end()) {
    throw uv_exception("Could not find stream buffer!");
  }
  buffer* ret = i->second;
  if (size > 0) ret->data.resize(size);
  mBuffers.erase(i);
  return ret;
};

};

};
