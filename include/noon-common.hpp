// Copyright 2016
// Gary Friedman

#pragma once

#include <condition_variable>
#include <map>

namespace noon
{

using condvar = std::condition_variable;
using mutex = std::mutex;
using lock = std::unique_lock<mutex>;

inline double now(clockid_t aClock = CLOCK_REALTIME)
{
  struct timespec tp;
  clock_gettime(aClock, &tp);
  return tp.tv_sec + (tp.tv_nsec / 1000000000.0);
};

template<typename K, typename V>
void map_append(std::map<K, V>&)
{};

template<typename K, typename V, typename... Ts>
void map_append(std::map<K, V>& aMap, K&& aKey, V&& aVal, Ts&&... aArgs)
{
  aMap.emplace(aKey, aVal);
  map_append<K, V>(aMap, std::forward<Ts>(aArgs)...);
};

template<typename K, typename V, typename... Ts>
std::map<K, V> map(K&& aKey, V&& aVal, Ts&&... aArgs)
{
  std::map<K, V> tMap;
  tMap.emplace(aKey, aVal);
  map_append<K, V>(tMap, std::forward<Ts>(aArgs)...);
  return tMap;
};

template<typename K, typename V>
void extend(std::map<K, V>& aMap, std::map<K, V>&& aNewMap)
{
  for (auto& tPair : aNewMap) {
    aMap.emplace(tPair.first, tPair.second);
  }
};

};
