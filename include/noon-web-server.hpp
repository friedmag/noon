// Copyright 2016
// Gary Friedman

#pragma once

#include "noon-uv-loop.hpp"

namespace noon {

class web_server;
class web_server_conn;

struct web_server_reqres_impl
{
  web_server_reqres_impl(web_server* aServer, web_server_conn* aConn)
    :mServer(aServer), mConn(aConn), mHeadersWritten(false), mCloseConn(false) {};
  web_server* mServer;
  web_server_conn* mConn;
  bool mHeadersWritten;
  bool mCloseConn;
};

struct web_server_request
{
  header_map headers;

  web_server_request(auto aImpl);

  using DataCallback = std::function<void(std::string)>;
  void onData(DataCallback aCb);

  using end_promise = promise<uv_exception>;
  end_promise::PtrWrap onEnd();

private:
  std::shared_ptr<web_server_reqres_impl> mImpl;
};
using web_server_request_ptr = std::shared_ptr<web_server_request>;

struct web_server_response
{
  int statusCode;
  header_map headers;

  template<typename... Ts>
  void setHeader(std::string&& aKey, std::string&& aVal, Ts&&... aArgs)
  {
    extend(headers, map(std::move(aKey), std::move(aVal)));
  }

  void removeHeader(std::string&& aKey)
  {
    headers.erase(aKey);
  }

  void writeHead();

  void writeHead(header_map&& aHeaders)
  {
    headers = aHeaders;
    writeHead();
  };

  void write();
  void write(const std::string& aData);
  
  void end()
  {
    write();
    if (mImpl->mCloseConn) {
      close();
    }
  };

  void end(const std::string& aData)
  {
    write(aData);
    if (mImpl->mCloseConn) {
      close();
    }
  };

  void close();

  web_server_response(auto aImpl) : statusCode(200), headers(HTTP_DEFAULT_HEADERS),
    mImpl(aImpl) {};

private:
  std::shared_ptr<web_server_reqres_impl> mImpl;
};
using web_server_response_ptr = std::shared_ptr<web_server_response>;

class web_server_conn
{
public:
  web_server_conn(web_server& aServer, std::unique_ptr<tcp>&& aTcp)
      :mServer(aServer), mTcp(std::move(aTcp)), mParser(this), valid(true)
  {
    mTcp->read(std::bind(&web_server_conn::onRead, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
  };

  ~web_server_conn() { valid = false; };

public:
  int onMessageBegin() {
    return 0;
  };

  int onUrl(std::string&& aStr)
  {
    mUrl = std::move(aStr);
    return 0;
  };

  int onStatus(std::string&&) {
    return 0;
  };

  int onHeaderField(std::string&& aStr) {
    printf("HEADER FIELD: %s\n", aStr.c_str());
    mHeaderField = std::move(aStr);
    printf("HEADER FIELD: %s\n", mHeaderField.c_str());
    return 0;
  };

  int onHeaderValue(std::string&& aStr) {
    printf("HEADER VALUE: %s\n", aStr.c_str());
    printf("HEADER FIELD: %s\n", mHeaderField.c_str());
    mHeaders.emplace(std::move(mHeaderField), std::move(aStr));
    return 0;
  };

  int onHeadersComplete();

  int onBody(std::string&& aStr) {
#ifdef __arm__
//    printf("BODY: (%u) %s\n", aStr.size(), aStr.c_str());
#else
//    printf("BODY: (%lu) %s\n", aStr.size(), aStr.c_str());
#endif
    if (mDataCb) mDataCb(aStr);
    return 0;
  };

  int onMessageComplete();

private:
  void onRead(CException, buffer_ptr, bool);

private:
  web_server& mServer;
  std::unique_ptr<tcp> mTcp;
  web_parser<web_server_conn> mParser;
  bool valid; // TODO: use or remove
  std::string mUrl;
  std::string mHeaderField;
  header_map mHeaders;

  web_server_request_ptr mReq;
  web_server_response_ptr mRes;

  web_server_request::DataCallback mDataCb;
  web_server_request::end_promise::Resolve mEnd;
  web_server_request::end_promise::Error mError;

  friend class web_server;
  friend class web_server_request;
  friend class web_server_response;
};

class web_server {
public:
  using callback = std::function<void(web_server_request_ptr, web_server_response_ptr)>;

  web_server(Loop& aLoop)
      :mTcp(aLoop) {};

  void listen(uint16_t aPort)
  {
    mTcp.bind(aPort);
    mTcp.listen(std::bind(&web_server::onConnect, this));
  };

  void close()
  {
    mTcp.close();
  };

  void get(std::string&& aUrl, callback aCb)
  {
    auto& tGets = mHandlers[HTTP_GET];
    tGets.emplace_back(std::make_tuple(aUrl, aCb));
  };

  void put(std::string&& aUrl, callback aCb)
  {
    auto& tPuts = mHandlers[HTTP_PUT];
    tPuts.emplace_back(std::make_tuple(aUrl, aCb));
  };

  void post(std::string&& aUrl, callback aCb)
  {
    auto& tPosts = mHandlers[HTTP_POST];
    tPosts.emplace_back(std::make_tuple(aUrl, aCb));
  };
  
  void remove(std::string&& aUrl, callback aCb)
  {
    auto& tDeletes = mHandlers[HTTP_DELETE];
    tDeletes.emplace_back(std::make_tuple(aUrl, aCb));
  };

private:
  void onConnect()
  {
    auto tResponse = std::make_unique<web_server_conn>(*this, mTcp.accept<tcp>());
    mResponses.emplace(tResponse.get(), std::move(tResponse));
  };

  void handle(web_server_conn& aConn, http_method aMethod, std::string&& aUrl)
  {
    auto tImpl = std::make_shared<web_server_reqres_impl>(this, &aConn);
    aConn.mReq = std::make_shared<web_server_request>(tImpl);
    aConn.mRes = std::make_shared<web_server_response>(tImpl);
    for (auto& tHandler : mHandlers[aMethod]) {
      auto& tUrl = std::get<0>(tHandler);
      if (aUrl == tUrl) {
        auto& tCb = std::get<1>(tHandler);
        tCb(aConn.mReq, aConn.mRes);
        return;
      }
    }
    aConn.mRes->statusCode = 404;
    aConn.mRes->end();
  };

  void remove(web_server_conn* aResponse)
  {
    // Defer removal to next loop iteration
    mTcp.loop().next([=]() {
      this->mResponses.erase(aResponse);
    });
  };

private:
  tcp mTcp;
  std::map<web_server_conn*, std::unique_ptr<web_server_conn>> mResponses;
  std::map<http_method, std::vector<std::tuple<std::string, callback>>> mHandlers;

  friend class web_server_conn;
  friend class web_server_response;
};

inline web_server_request::web_server_request(auto aImpl)
    : headers(std::move(aImpl->mConn->mHeaders)), mImpl(aImpl) {};

inline void web_server_request::onData(web_server_request::DataCallback aCb)
{
  mImpl->mConn->mDataCb = aCb;
  printf("SENDING CONTINUE???\n");
  if (headers["Expect"] == "100-continue") {
    printf("SENDING CONTINUE!!!\n");
    mImpl->mConn->mTcp->write(std::string("HTTP/1.1 100 Continue\r\n\r\n"));
  }
};

inline web_server_request::end_promise::PtrWrap web_server_request::onEnd()
{
  return end_promise::create([this](auto aResolve, auto aError) {
    mImpl->mConn->mEnd = aResolve;
    mImpl->mConn->mError = aError;
  });
};

inline int web_server_conn::onHeadersComplete()
{
  mServer.handle(*this, static_cast<http_method>(mParser.mParser.method), std::move(mUrl));
  return 0;
};

inline int web_server_conn::onMessageComplete()
{
  if (mEnd) mEnd();
  return 0;
};

inline void web_server_conn::onRead(CException e, buffer_ptr aData, bool aEof)
{
  if (e) {
    mServer.remove(this);
    return;
  }
  if (aEof) {
    mParser.execute();
  } else {
    mParser.execute(aData.get());
  }
};

inline void web_server_response::writeHead()
{
  std::stringstream tOut;
  tOut << "HTTP/1.1 " << statusCode << " ";
  {
    auto tFind = HTTP_STATUS_CODES.find(statusCode);
    if (tFind != HTTP_STATUS_CODES.end()) {
      tOut << (*tFind).second;
    } else {
      tOut << "Unknown";
    }
  }
  tOut << "\r\n";
  for (auto& tHeader : headers) {
    tOut << tHeader.first << ": " << tHeader.second << "\r\n";
    if (tHeader.first == "Connection" && tHeader.second == "close")
      mImpl->mCloseConn = true;
  }
  tOut << "\r\n";
  mImpl->mConn->mTcp->write(tOut.str());
  mImpl->mHeadersWritten = true;
};

inline void web_server_response::write()
{
  if (!mImpl->mHeadersWritten) {
    writeHead();
  }
}

inline void web_server_response::write(const std::string& aData)
{
  if (!mImpl->mHeadersWritten) {
    writeHead();
  }
  mImpl->mConn->mTcp->write(aData);
};

inline void web_server_response::close()
{
  // TODO: move to destructor?
  mImpl->mConn->mTcp->close();
  mImpl->mServer->remove(mImpl->mConn);
};

};
