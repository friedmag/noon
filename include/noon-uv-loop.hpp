// Copyright 2016
// Gary Friedman

#pragma once

#include "noon-common.hpp"
#include <uv.h>
#include <string>
#include <functional>
#include <memory>
#include <chrono>

namespace noon
{

class Util {
  public:
    static inline uint64_t hrtime() { return uv_hrtime(); };
};

class Timer;

struct AsyncRequest;

class Loop {
  public:
    using NextCallback = std::function<void ()>;
    using idle_key = size_t;
    using close_cb = std::function<void ()>;
    using idle_cb = std::function<void (idle_key)>;

    enum class Run {
        DEFAULT = UV_RUN_DEFAULT,
        ONCE = UV_RUN_ONCE,
        NOWAIT = UV_RUN_NOWAIT,
    };

    Loop(uv_loop_t* loop = NULL);
    ~Loop();

    uv_loop_t* loop();

    bool run(Run flag = Run::DEFAULT);
    bool run_once()
    { return run(Run::ONCE); };
    bool run_nowait()
    { return run(Run::NOWAIT); };

    void idle(idle_cb cb);
    void cancel(idle_key key);
    void next(NextCallback cb);
    void exec(NextCallback cb);

    bool currentThread() const;

    void callAsync();

  private:
    struct Internal;

    // TODO: Fix this hack allowing loops to be copyable!!
    // Do something more intentional, at least!
    std::shared_ptr<Internal> internal_;
};

class Timer
{
  public:
    using TimerCallback = std::function<void ()>;

    Timer(Loop& loop, bool unref = false);
    ~Timer();

    void start(std::chrono::milliseconds timeout,
            std::chrono::milliseconds repeat, TimerCallback cb);
    void stop();
    void again();
    void setRepeat(std::chrono::milliseconds repeat);
    int64_t getRepeat();

    inline uv_timer_t* timer() { return &timer_; };

  private:
    uv_timer_t timer_;
    Loop*      loop_;

    Timer();
};

};
