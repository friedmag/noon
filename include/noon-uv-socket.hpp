// Copyright 2016
// Gary Friedman

#pragma once

#include "noon-uv-stream.hpp"

namespace noon
{

using ResolvePromise = promise<uv_exception, std::string>;

class socket
{
  public:
    socket(Loop& loop);
    virtual ~socket() {};

    ResolvePromise::PtrWrap resolve(const std::string& name);

    struct Address {
      std::string addr;
      uint16_t    port;

      inline void ip4(struct sockaddr_in& saddr)
      { uv_ip4_addr(addr.c_str(), port, &saddr); };

      inline Address(const struct sockaddr* addr) : Address((uint16_t)0) {
        if (addr != NULL) {
          char host[128];
          this->port = ntohs(((struct sockaddr_in*)addr)->sin_port);
          uv_ip4_name((struct sockaddr_in*)addr, host, 128);
          this->addr = host;
        }
      };

      inline Address(const std::string& addr, uint16_t port = 0)
      { this->addr = addr; this->port = port; };

      inline Address(uint16_t port = 0)
      { this->port = port; };

      inline bool operator<(const Address& o) const
      { return (addr < o.addr || (addr == o.addr && port < o.port)); };

      inline bool operator==(const Address& o) const
      { return (addr == o.addr && port == o.port); };
    };

    virtual Address address() = 0;
    virtual Address peer() = 0;

  protected:
    Loop& mLoop;
};

std::ostream& operator<<(std::ostream& os, const socket::Address& addr);

// TODO: make this a moveable type? It's pointer-based, so that would be very easy.
class tcp : public socket, public stream
{
  public:
    tcp(Loop& loop);

    inline uv_tcp_t* raw() { return mTcp.get(); };

    uv_exception bind(const Address& aAddr);
    inline uv_exception bind(const std::string& host, uint16_t port = 0)
    { return bind(Address(host, port)); };
    inline uv_exception bind(uint16_t port = 0)
    { return bind(Address("0.0.0.0", port)); };

    ConnectPromise::PtrWrap connect(const Address& address);
    ConnectPromise::PtrWrap connect(const std::string& host, uint16_t port)
    { return connect(Address(host, port)); };

    Address address() override;
    Address peer() override;

  private:
    ptr_wrap<uv_tcp_t> mTcp;

    tcp() = delete;
};

using FullReadCallback = std::function<void (CException, buffer_ptr, const socket::Address&)>;

class udp : public socket {
  public:

    udp(Loop& loop);
    ~udp();

    uv_exception bind(const Address& aAddr);
    inline uv_exception bind(const std::string& host, uint16_t port = 0)
    { return bind(Address(host, port)); };
    inline uv_exception bind(uint16_t port = 0)
    { return bind(Address("0.0.0.0", port)); };

    void broadcast(const Address& bcastAddr);
    inline void broadcast(uint16_t port) { broadcast(Address("255.255.255.255", port)); };
    void target(const Address& addr);

    void read(ReadCallback cb);
    void readFull(FullReadCallback cb);
    void readStop();

    void write(const std::string* str, WriteCallback cb = WriteCallback());
    void write(const std::string& str, WriteCallback cb = WriteCallback());
    void write(const void* buf, size_t len, WriteCallback cb = WriteCallback());

    /**
     * Closes the socket
     */
    void close(CloseCallback cb = CloseCallback());

    Address address() override;
    Address peer() override;

  private:
    std::unique_ptr<uv_udp_t>       mUdp;
    std::unique_ptr<buffer_manager> mBufferManager;

    // TODO: Add NONCOPYABLE/etc macro to ctools
    udp() = delete;
};

};
