// Copyright 2016
// Gary Friedman

#pragma once

#include <experimental/tuple>
#include <memory>
#include <iostream>
#include <functional>

namespace noon {

#define NOON_PROMISE_DEFAULT_ERROR_HANDLER \
    if (!mErrorHolder) { \
      mErrorHolder = new Error([=](exception aErr) { \
        bool tSuccess = ret->onError(aErr); \
        delete shared->mThenHolder; \
        auto tErrorHolder = mErrorHolder; \
        mErrorHolder = nullptr; \
        delete tErrorHolder; \
        return tSuccess; \
      }); \
    }

//////////////////////////////////////////////////////////////////////////
// BASE CASE
//////////////////////////////////////////////////////////////////////////
template<typename exception, typename ... T>
class promise : public std::enable_shared_from_this<promise<exception, T...>> {
public:
  using P = promise<exception, T...>;
  using Ptr = std::shared_ptr<P>;
  using Tuple = std::tuple<T...>;
  using Resolve = std::function<void(T...)>;
  using Error = std::function<bool(exception)>;
  using Executor = std::function<void(Resolve, Error)>;

  class PtrWrap {
  public:
    PtrWrap()
      :mPtr(std::make_shared<P>(Executor()))
    {
    };

    PtrWrap(Executor aExecutor)
      :mPtr(std::make_shared<P>(Executor())),
       mExecutor(aExecutor)
    {
    };

    PtrWrap(PtrWrap&& other)
      :mPtr(std::move(other.mPtr)), mExecutor(std::move(other.mExecutor))
    {
    };

    ~PtrWrap() noexcept(false)
    {
      if (mExecutor) {
        // Make sure the pointer remains in scope/attached to the closures
        auto tPtr = mPtr;
        mExecutor([=](T... aVals) {
          tPtr->onResolve(aVals...);
        },
        [=](exception aException) {
          return tPtr->onError(aException);
        });
      }
    };

    P* operator->() const
    { return mPtr.get(); };

  private:
    Ptr mPtr;
    Executor mExecutor;
  };

  static PtrWrap create(Executor aExecutor)
  {
    return PtrWrap(aExecutor);
  };

  promise<exception, T...>(Executor aExecutor)
    :mThenHolder(nullptr), mErrorHolder(nullptr)
  {
    if (aExecutor)
      aExecutor([=](T... aVals) {
        onResolve(aVals...);
      },
      [=](exception aException) {
        return onError(aException);
      });
  };

  Ptr then(std::function<PtrWrap(T...)> aFunc)
  {
    auto ret = std::make_shared<P>(typename P::Executor());
    auto shared = this->shared_from_this();
    mThenHolder = new Resolve([=](T... aVals) {
      try {
        auto tNewPromise(std::move(aFunc(aVals...)));
        tNewPromise->mThenHolder = ret->mThenHolder;
        tNewPromise->mErrorHolder = ret->mErrorHolder;
        delete shared->mErrorHolder;
        delete mThenHolder;
      } catch (exception& e) {
        if (!onError(e)) throw e;
      }
    });

    NOON_PROMISE_DEFAULT_ERROR_HANDLER;

    return ret;
  };

  Ptr then(std::function<Ptr(T...)> aFunc)
  {
    auto ret = std::make_shared<P>(typename P::Executor());
    auto shared = this->shared_from_this();
    mThenHolder = new Resolve([=](T... aVals) {
      try {
        auto tNewPromise = aFunc(aVals...);
        if (tNewPromise) {
          tNewPromise->mThenHolder = ret->mThenHolder;
          tNewPromise->mErrorHolder = ret->mErrorHolder;
        } else {
          ret->onResolve(aVals...);
        }
        delete shared->mErrorHolder;
        delete mThenHolder;
      } catch (exception& e) {
        if (!onError(e)) throw e;
      }
    });

    NOON_PROMISE_DEFAULT_ERROR_HANDLER;

    return ret;
  };

  Ptr then(std::function<Tuple(T...)> aFunc)
  {
    auto ret = std::make_shared<P>(typename P::Executor());
    auto shared = this->shared_from_this();
    mThenHolder = new Resolve([=](T... aVals) {
      try {
        const Tuple& tVals = aFunc(aVals...);
        // Cascade success
        std::experimental::apply([=](T... aVals) {
            ret->onResolve(aVals...);
          }, tVals);
        delete shared->mErrorHolder;
        delete mThenHolder;
      } catch (exception& e) {
        if (!onError(e)) throw e;
      }
    });

    NOON_PROMISE_DEFAULT_ERROR_HANDLER;

    return ret;
  };

  template<typename... rT>
  typename promise<exception, rT...>::Ptr then(std::function<typename promise<exception, rT...>::PtrWrap(T...)> aFunc)
  {
    using NewPromise = promise<exception, rT...>;
    auto ret = std::make_shared<NewPromise>(typename NewPromise::Executor());
    auto shared = this->shared_from_this();
    mThenHolder = new Resolve([=](T... aVals) {
      try {
        auto tNewPromise(std::move(aFunc(aVals...)));
        tNewPromise->mThenHolder = ret->mThenHolder;
        tNewPromise->mErrorHolder = ret->mErrorHolder;
        delete shared->mErrorHolder;
        delete mThenHolder;
      } catch (exception& e) {
        if (!onError(e)) throw e;
      }
    });

    NOON_PROMISE_DEFAULT_ERROR_HANDLER;

    return ret;
  };

  template<typename... rT>
  typename promise<exception, rT...>::Ptr then(std::function<typename promise<exception, rT...>::Ptr(T...)> aFunc)
  {
    using NewPromise = promise<exception, rT...>;
    auto ret = std::make_shared<NewPromise>(typename NewPromise::Executor());
    auto shared = this->shared_from_this();
    mThenHolder = new Resolve([=](T... aVals) {
      try {
        auto tNewPromise = aFunc(aVals...);
        if (tNewPromise) {
          tNewPromise->mThenHolder = ret->mThenHolder;
          tNewPromise->mErrorHolder = ret->mErrorHolder;
        }
        delete shared->mErrorHolder;
        delete mThenHolder;
      } catch (exception& e) {
        if (!onError(e)) throw e;
      }
    });

    NOON_PROMISE_DEFAULT_ERROR_HANDLER;

    return ret;
  };

  Ptr error(std::function<Ptr(exception)> aFunc)
  {
    auto ret = std::make_shared<P>(typename P::Executor());
    auto shared = this->shared_from_this();

    mErrorHolder = new Error([=](exception aErr) {
      try {
        auto tNewPromise = aFunc(aErr);
        if (tNewPromise) {
          tNewPromise->mThenHolder = ret->mThenHolder;
          tNewPromise->mErrorHolder = ret->mErrorHolder;
        } else {
          Tuple tTuple;
          std::experimental::apply([=](T... aVals) {
              ret->onResolve(aVals...);
            }, tTuple);
        }
        delete shared->mThenHolder;
        delete mErrorHolder;
      } catch (exception& e) {
        if (!ret->onError(e)) throw e;
      }
      return true;
    });

    return ret;
  };

  Ptr error(std::function<PtrWrap(exception)> aFunc)
  {
    auto ret = std::make_shared<P>(typename P::Executor());
    auto shared = this->shared_from_this();

    mErrorHolder = new Error([=](exception aErr) {
      try {
        auto tNewPromise = aFunc(aErr);
        tNewPromise->mThenHolder = ret->mThenHolder;
        tNewPromise->mErrorHolder = ret->mErrorHolder;
        delete shared->mThenHolder;
        delete mErrorHolder;
      } catch (exception& e) {
        if (!ret->onError(e)) throw e;
      }
      return true;
    });

    return ret;
  };

  template<typename... rT>
  typename promise<exception, rT...>::Ptr error(std::function<typename promise<exception, rT...>::PtrWrap(exception)> aFunc)
  {
    using NewPromise = promise<exception, rT...>;
    auto ret = std::make_shared<NewPromise>(typename NewPromise::Executor());
    auto shared = this->shared_from_this();
    mErrorHolder = new Error([=](exception aErr) {
      try {
        auto tNewPromise(std::move(aFunc(aErr)));
        tNewPromise->mThenHolder = ret->mThenHolder;
        tNewPromise->mErrorHolder = ret->mErrorHolder;
        delete shared->mThenHolder;
        delete mErrorHolder;
      } catch (exception& e) {
        if (!ret->onError(e)) throw e;
      }
      return true;
    });
    return ret;
  };

  template<typename... rT>
  typename promise<exception, rT...>::Ptr error(std::function<typename promise<exception, rT...>::Ptr(exception)> aFunc)
  {
    using NewPromise = promise<exception, rT...>;
    auto ret = std::make_shared<NewPromise>(typename NewPromise::Executor());
    auto shared = this->shared_from_this();
    mErrorHolder = new Error([=](exception aErr) {
      try {
        auto tNewPromise = aFunc(aErr);
        if (tNewPromise) {
          tNewPromise->mThenHolder = ret->mThenHolder;
          tNewPromise->mErrorHolder = ret->mErrorHolder;
        } else {
          typename NewPromise::Tuple tTuple;
          std::experimental::apply([=](rT... aVals) {
              ret->onResolve(aVals...);
            }, tTuple);
        }
        delete shared->mThenHolder;
        delete mErrorHolder;
      } catch (exception& e) {
        if (!ret->onError(e)) throw e; // TODO: delete?
      }
      return true;
    });
    return ret;
  };

public: // TODO: private
  void onResolve(T... aVals)
  {
    if (mThenHolder != nullptr)
      (*mThenHolder)(aVals...);
  };

  bool onError(exception aErr)
  {
    if (mErrorHolder != nullptr)
      return (*mErrorHolder)(aErr);
    return false;
  };

  Resolve* mThenHolder;
  Error* mErrorHolder;
};

template<typename ... T>
using basic_promise = promise<std::exception, T...>;

};
