// Copyright 2016
// Gary Friedman

#pragma once

#include "noon-uv-pipe.hpp"
#include "noon-uv-socket.hpp"

namespace noon {

struct web_client_response {
  int statusCode;
  std::string statusMessage;
  header_map headers;
  std::string body;
};
using web_client_response_ptr = std::shared_ptr<web_client_response>;

class web_client_conn {
public:
    web_client_conn(Loop& aLoop, options&& aOptions)
      :mParser(this), mResponse(std::make_shared<web_client_response>()),
        mOptions(std::move(aOptions))
    {
        http_parser_url tResult;
        mOptions.try_emplace("method", "GET");
        auto tUrl = mOptions.at("url");
        if (tUrl.substr(0, 7) == "file://") {
            auto tPos = tUrl.substr(7).find(':');
            auto tSockPath = tUrl.substr(7, tPos);
            auto tPath = tUrl.substr(8 + tPos);
            mStream = std::make_unique<pipe>(aLoop);
            static_cast<pipe*>(mStream.get())->connect(tSockPath)
              ->then(std::bind(&web_client_conn::onConnect, this, tPath));
        } else if (!http_parser_parse_url(tUrl.c_str(), tUrl.size(), 0, &tResult)) {
            std::string tSchema = tUrl.substr(
                    tResult.field_data[UF_SCHEMA].off,
                    tResult.field_data[UF_SCHEMA].len);
            std::string tHost = tUrl.substr(
                    tResult.field_data[UF_HOST].off,
                    tResult.field_data[UF_HOST].len);

            if (tSchema != "http") {
                throw uv_exception("Unknown schema: " + tSchema);
            }

            mStream = std::make_unique<tcp>(aLoop);
            static_cast<tcp*>(mStream.get())->connect(tHost, tResult.port)
              ->then(std::bind(&web_client_conn::onConnect, this,
                        tUrl.substr(
                            tResult.field_data[UF_PATH].off,
                            tResult.field_data[UF_PATH].len)));
        }
    };

    ~web_client_conn()
    {
    };

    using response_promise = promise<uv_exception, web_client_response_ptr>;
    auto response()
    {
      return response_promise::create([this](auto aResolve, auto aError) {
        mResponseResolve = aResolve;
        mResponseError = aError;
      });
    };

    auto body(std::string&& aBody) {
      mBody = std::move(aBody);
      return this;
    };

private:
  ConnectPromise::Ptr onConnect(const std::string& aPath) {
    std::stringstream tStr;
    tStr << mOptions.at("method") << " " << aPath << " HTTP/1.1\r\n\r\n";
    if (!mBody.empty())
      tStr << mBody;
    // TODO: headers
    mStream->write(tStr.str(), [this](auto& e) {
      if (e) {
        return;
      }
      mStream->read([this](auto& e, auto buf, auto eof) {
        if (e) {
          return;
        }
        if (eof) {
          mParser.execute();
        } else {
          mParser.execute(buf.get());
        }
      });
    });
    return nullptr;
  };

public:
  int onMessageBegin() {
    return 0;
  };

  int onUrl(std::string&&) {return 0;};

  int onStatus(std::string&& aStr) {
    mResponse->statusCode = mParser.status_code();
    mResponse->statusMessage = std::move(aStr);
    return 0;
  };

  int onHeaderField(std::string&& aStr) {
    mHeaderField = std::move(aStr);
    return 0;
  };

  int onHeaderValue(std::string&& aStr) {
    mResponse->headers.emplace(std::move(mHeaderField), std::move(aStr));
    return 0;
  };

  int onHeadersComplete() {
    return 0;
  };

  int onBody(std::string&& aStr) {
    mResponse->body = aStr;
    return 0;
  };

  int onMessageComplete() {
    mResponseResolve(mResponse);
    return 0;
  };

private:
  std::unique_ptr<stream> mStream;
  web_parser<web_client_conn> mParser;
  response_promise::Resolve mResponseResolve;
  response_promise::Error mResponseError;
  web_client_response_ptr mResponse;
  std::string mHeaderField;
  options mOptions;
  std::string mBody;
};

using web_client_conn_ptr = std::unique_ptr<web_client_conn>;

class web_client {
public:
    using response_promise = promise<uv_exception, web_client_response_ptr>;
    
    web_client(Loop& aLoop)
        :mLoop(aLoop)
    {
    };

    web_client_conn_ptr connect(std::string&& aUrl) {
      options tOptions;
      tOptions["url"] = std::move(aUrl);
      return std::make_unique<web_client_conn>(mLoop, std::move(tOptions));
    };
    
    auto get(std::string&& aUrl) {
      return response_promise::create(
		      [this, tUrl{std::move(aUrl)}]
		      (auto resolve, auto) mutable {
        options tOptions;
        tOptions.emplace("url", std::move(tUrl));
        tOptions.emplace("method", "GET");
        // Should be unique_ptr, but can't put that into the std::function of the promise,
        // as it must be copyable
        auto tConn = std::make_shared<web_client_conn>(mLoop, std::move(tOptions));
        tConn->response()->then([tConn, resolve](auto aRes) {
            printf("RESPONSE! '%s'\n", aRes->body.c_str());
            resolve(aRes);
            return web_client_conn::response_promise::Ptr();
        });
      });
    };
    
    // TODO: allow lambda for data?
    auto post(std::string&& aUrl, std::string&& aData) {
      return response_promise::create(
		      [this, tUrl{std::move(aUrl)}, tData{std::move(aData)}]
		      (auto resolve, auto) mutable {
        options tOptions;
        tOptions.emplace("url", std::move(tUrl));
        tOptions.emplace("method", "POST");
        // Should be unique_ptr, but can't put that into the std::function of the promise,
        // as it must be copyable
        auto tConn = std::make_shared<web_client_conn>(mLoop, std::move(tOptions));
        tConn->body(std::move(tData));
        tConn->response()->then([tConn, resolve](auto aRes) {
            printf("RESPONSE! '%s'\n", aRes->body.c_str());
            resolve(aRes);
            return web_client_conn::response_promise::Ptr();
        });
      });
    };
    
    auto put(std::string&& aUrl) {
      return response_promise::create([=, tUrl{std::move(aUrl)}](auto, auto) mutable {
        auto tConn = this->connect(std::move(tUrl));
      });
    };
    
    auto remove(std::string&& aUrl) {
      return response_promise::create([=, tUrl{std::move(aUrl)}](auto, auto) mutable {
        auto tConn = this->connect(std::move(tUrl));
      });
    };

private:
    Loop&   mLoop;
};

class web {
public:
  web(Loop& aLoop)
    :mLoop(aLoop) {};

  web_client_conn_ptr request(options&& aOptions)
  {
    // TODO: options, w/ a nice breakout
    auto tReq = std::make_unique<web_client_conn>(mLoop, std::move(aOptions));
    return tReq;
  };

private:
  Loop& mLoop;
};

};
