// Copyright 2016
// Gary Friedman

#pragma once

#include "noon-uv-stream.hpp"

namespace noon
{

class Loop;

class pipe : public stream
{
  public:
    pipe(Loop& loop, bool ipc = false);

    /**
     */
    uv_exception bind(const std::string& aName);

    /**
     */
    ConnectPromise::PtrWrap connect(const std::string& aName);

    std::string address() const;
    std::string peer() const;

    /**
     */
    int pending();

    /**
     */
    std::unique_ptr<stream> get_pending();

  private:
    ptr_wrap<uv_pipe_t> mPipe;
};

};
