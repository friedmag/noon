// Copyright 2016
// Gary Friedman

#pragma once

#include "noon-promise.hpp"
#include "noon-uv-buffer.hpp"
#include "noon-uv-common.hpp"
#include <vector>
#include <cstring>

namespace noon
{

class Loop;

using data_type      = std::vector<char>;
using data_list      = std::vector<data_type>;
using StatusCallback = std::function<void (CException)>;
using WriteCallback  = StatusCallback;
using ReadCallback   = std::function<void (CException, buffer_ptr, bool)>;
using NewReadCallback   = std::function<void (CException)>;
using CloseCallback  = std::function<void ()>;
using ListenCallback = StatusCallback;

using ConnectPromise = promise<uv_exception>;
using ReadPromise = promise<uv_exception>;
using WritePromise = promise<uv_exception>;

/**
 * ptr_wrap
 *
 * Very basic pointer wrapping class.  This is used in tcp/udp to keep a typed
 * pointer, while stream has it as a unique_ptr.
 * TODO: is this actually needed, vs. keeping a raw pointer?
 * TODO: Move this to common?
 */
template<class T>
struct ptr_wrap {
    ptr_wrap(T* aPtr) : mPtr(aPtr) {};
    T* get() { return mPtr; };
    const T* get() const { return mPtr; };
    T* mPtr;
};

// TODO: Could template this to the proper uv_type to minimize needs upstream
class stream
{
  public:
    stream(Loop& aLoop, uv_stream_t* aStream);
    virtual ~stream();

    // for convenience
    void write(const std::string& str, WriteCallback cb = WriteCallback())
    {
        data_type tWrap(str.size());
        memcpy(tWrap.data(), str.data(), str.size());
        write(tWrap, cb);
    };

    /**
     * Writes data to the stream
     * @param str Data to be written (data will not be COPIED nor DELETED)
     * @param cb  Callback to call upon completion
     */
    void write(const data_type* str, WriteCallback cb = WriteCallback());

    /**
     * Writes data to the stream
     * @param str Data to be written (WILL BE DELETED WHEN COMPLETE)
     * @param cb  Callback to call upon completion
     */
    void write(std::unique_ptr<data_type> str, WriteCallback cb = WriteCallback());

    /**
     * Writes data to the stream
     * @param str Data to be written (data will be COPIED)
     * @param cb  Callback to call upon completion
     */
    void write(const data_type& str, WriteCallback cb = WriteCallback());
    /**
     * Writes data to the stream
     * @param buf Data to be written (data will be COPIED)
     * @param len Amount of data to be written/copied
     * @param cb  Callback to call upon completion
     */
    void write(const void* buf, size_t len, WriteCallback cb = WriteCallback());

    /**
     * Writes data to the stream
     * @param aBuf Data to be written (data will be COPIED)
     * @return WritePromise
     */
    template<typename T>
    auto write(T&& aBuf)
    {
      // TODO: make this not need to copy
      return WritePromise::create([&](auto aResolve, auto aError) {
        this->write(aBuf.data(), aBuf.size(), [=](auto aException) {
          if (aException) aError(aException);
          else aResolve();
        });
      });
    };

    /**
     */
    void read(data_type& buf, NewReadCallback cb);
    // TODO: cleanup
    ReadPromise::Ptr read(data_type& buf);
    /**
     */
    void read(ReadCallback cb);
    /**
     */
    void readStop();

    /**
     */
    uv_exception listen(ListenCallback aCb);

    /**
     */
    template<class T, class... A>
    std::unique_ptr<T> accept(A&&... a) {
        auto tRet = std::make_unique<T>(loop(), std::forward<A>(a)...);
        uv_accept(mStream.get(), tRet->mStream.get());
        return tRet;
    };

    /**
     * Closes the stream
     */
    void close(CloseCallback cb = CloseCallback());

    Loop& loop() { return *mLoop; };

    uv_stream_t* raw() { return mStream.get(); };

  protected:
    Loop*                           mLoop;
    std::unique_ptr<uv_stream_t>    mStream;
    std::unique_ptr<buffer_manager> mBufferManager;
};

};
